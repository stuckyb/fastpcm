#!/usr/bin/Rscript

library('ape')
library('optparse')


# Calculates the expected size of an ape tree with a given number of leaf
# nodes, using the known structure of ape's tree data type and the sizes of
# basic types in R.  Basically, this is a check on the results of object.size()
# for a tree object.  The results of this function are extremely close to those
# returned by object.size for trees up to at least 100,000 tips (they typically
# differ by only 4 bytes).  However, for a tree with 1,000,000 tips, the result
# of object.size is about 7 MiB larger.  I suspect the reason for that is that
# many of the tip labels in a very large tree are longer strings that require
# more than the minimum string size allocation.
estimateApeTreeSize = function(ntips) {
    # Per the documentation, an integer in R is 4 bytes and a numeric (double)
    # value is 8 bytes.
    n_sz = 8
    i_sz = 4

    # Get the base vector size.
    t1 = c(1.0,1.0)
    v_sz = object.size(t1) - (n_sz * 2)

    # Get the size of a (short) string.
    str_sz = object.size(c('test')) - v_sz

    # Get the base size of a 2-column matrix.
    mat_sz = object.size(matrix(c(1.0,1.0), ncol=2, nrow=1)) - (n_sz * 2)

    # Calculate the size of the labels vector...
    tsize = str_sz * ntips + v_sz

    # the edge lengths vector...
    tsize = tsize + n_sz * (ntips * 2 - 2) + v_sz

    # the edges table...
    tsize = tsize + i_sz * (ntips * 2 - 2) * 2 + mat_sz

    # the node count and root edge length...
    tsize = tsize + i_sz * 2 + v_sz + n_sz + v_sz
    
    # and the size of the containing list.
    tmp_list = list(elem1=c(),elem2=c(),elem3=c(),elem4=c(),elem5=c())
    class(tmp_list) = 'test'
    attr(tmp_list, 'attr2') = 'test2'

    tsize = tsize + object.size(tmp_list)

    return(tsize)
}


optslist = list(
    make_option(
        c('-m','--mode'), type='character', default='wait',
        help='The execution mode.  The default, "wait", means wait for terminal input before exiting, "data" means print data about tree size to the console and exit.  "wait" mode is intended for empirical study of process memory allocation.'
    ),
    make_option(
        c('-n','--numtrees'), type='integer', default=1,
        help='The number of tree instances to load (default = 1).'
    ),
    make_option(
        c('-l','--library'), type='character', default='ape',
        help='The tree library to use.  The only valid option is "ape".'
    )
)
argp = OptionParser(
    usage='%prog [options] treefile', option_list=optslist,
    description='Tests the memory footprint of phylogenetic tree implementations in R.'
)
args = parse_args(argp, positional_arguments=1)

numtrees = args$options$numtrees
p_library = args$options$library
treefile = args$args[1]

if (!file.exists(treefile)) {
    stop('Could not find the tree file, "', treefile, '".')
}

if (numtrees == 0) {
    # Running a for loop appears to add considerably to the memory footprint,
    # so we need to include that in the base memory use estimate.
    for (cnt in 1:1) {
    }
}

# Getting empirical memory estimates for objects in an R process is not easy.
# Even when drilling down to heap allocation information found in /proc/PID
# files, in some cases, allocating more trees actually *reduces* the heap size,
# presumably due to internal memory management magic (I've not invested the
# time to figure out exactly what is going on).  It appears that allocating a
# set of trees helps "warm up" heap management and reduces some of this noise,
# hence the following code.
if (args$options$mode != 'data') {
    dummy_trees = list()
    for (cnt in 1:10) {
        if (p_library == 'ape') {
            dummy_trees[[cnt]] = read.tree(file='test_data/rtree-2000_tips.new')
        } else {
            stop('Unrecognized phylogenetic library: "', p_library, '".')
        }
    }
}

# Extract the number of leaf nodes from the tree file name.
ntips = sub('.*rtree-', '', treefile, fixed=FALSE)
ntips = as.integer(sub('_tips.new', '', ntips, fixed=TRUE))

trees = list()

if (numtrees > 0) {
    for (cnt in 1:numtrees) {
        if (args$options$mode != 'data') {
            cat('Loading tree ', cnt, ' of ', numtrees, '...', sep='')
        }

        if (p_library == 'ape') {
            trees[[cnt]] = read.tree(file=treefile)
        } else {
            stop('Unrecognized phylogenetic library: "', p_library, '".')
        }

        if (args$options$mode != 'data') {
            cat(' done.\n')
        }
    }
}

# Force a garbage collection to try to reduce some of the results variability.
invisible(gc(FALSE))

if (args$options$mode == 'data') {
    cat('{"observed": ', object.size(trees[[1]]), ', ', sep='')
    cat('"expected": ', estimateApeTreeSize(ntips), '}\n', sep='')
} else {
    cat('Press enter to terminate the program.\n')
    invisible(scan('stdin', character(), nlines=1, quiet=TRUE))
}

