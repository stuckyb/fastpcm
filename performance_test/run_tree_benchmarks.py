#!/usr/bin/python3

import sys
import os.path
import csv
from argparse import ArgumentParser

scriptdir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(scriptdir, 'src-python'))
import tree_benchmarks
from helper_funcs import getTreeFiles, checkOutputDir


def getOutputPaths(basename, num_reps, output_dir='results'):
    """
    Prepares the results file paths, and returns the tuple
    (fullresults_path, summary_path).
    """
    checkOutputDir(output_dir)

    if basename == '':
        basename = 'tree_benchmarks-{0}_reps-{{0}}.csv'.format(num_reps)
    else:
        basename = basename + '-{0}.csv'

    fr_path = os.path.join(output_dir, basename.format('full_results'))
    sr_path = os.path.join(output_dir, basename.format('summary'))

    if os.path.exists(fr_path):
        exit('\nThe output file {0} already exists.\n'.format(fr_path))

    if os.path.exists(sr_path):
        exit('\nThe output file {0} already exists.\n'.format(sr_path))

    return (fr_path, sr_path)


argp = ArgumentParser(
    description='Runs benchmarks for tree implementations in Python.'
)
argp.add_argument(
    '-o', '--output', type=str, required=False, default='',
    help='The base name for the output CSV files (default: '
    '"tree_benchmark_results-N_reps").'
)
argp.add_argument(
    '-n', '--num_reps', type=int, required=False, default=1,
    help='The number of repetitions to run for each benchmark.'
)
argp.add_argument(
    '-s', '--sig_figs', type=int, required=False, default=0,
    help='The number of significant figures to report in the summary results '
    'file.  If sig_figs is <1, the summary values will reported at maximum '
    'precision.  Integer values are never rounded.  The default is 0; i.e., '
    'maximum precision.'
)
argp.add_argument(
    '-i', '--includes', type=str, required=False, default='all',
    help='A comma-separated list of benchmarks to run.'
)
argp.add_argument(
    '-e', '--excludes', type=str, required=False, default='',
    help='A comma-separated list of benchmarks to exclude.'
)
argp.add_argument(
    '-p', '--print_all', action='store_true', help='Print all supported '
    'benchmark names and exit.'
)
argp.add_argument(
    'tree_sizes', type=int, nargs='*', default=[1,2,4,10,100,1000,10000],
    help='The tree sizes to test.  Tree sizes are specified as the number of '
    'leaf nodes in a tree, in thousands (e.g., "1" means 1,000 leaf nodes).  '
    'If no tree sizes are specified, a default set of sizes is used: 1,000, '
    '2,000, 4,000, 10,000, 100,000, 1,000,000, 10,000,000.'
)
args = argp.parse_args()


# Get instances of all required benchmark classes.
tbms = tree_benchmarks.getBenchmarkers(['all'])

if args.print_all:
    for tbm in tbms:
        tbm.printBenchmarks()
        print()
    exit()


fr_path, sr_path = getOutputPaths(args.output, args.num_reps)

tree_sizes = sorted([size * 1000 for size in args.tree_sizes])
tree_files = getTreeFiles(tree_sizes)

# Process any explicit includes.
if args.includes not in ('', 'all'):
    include_list = [bmark.strip() for bmark in args.includes.split(',')]
    for tbm in tbms:
        tbm.setIncludes(include_list)

# Process any explicit excludes.
if args.excludes != '':
    exclude_list = [bmark.strip() for bmark in args.excludes.split(',')]
    for tbm in tbms:
        tbm.setExcludes(exclude_list)

colnames = ['leaf_node_cnt']
for tbm in tbms:
    colnames.extend(tbm.getColNames())

# Run each benchmark and write the results.
with open(fr_path, 'w') as fr_out, open(sr_path, 'w') as sr_out:
    fr_writer = csv.DictWriter(fr_out, ['leaf_node_cnt', 'benchmark', 'time'])
    fr_writer.writeheader()

    sr_writer = csv.DictWriter(sr_out, colnames)
    sr_writer.writeheader()

    fr_res = {}
    for treesize in tree_sizes:
        fr_res['leaf_node_cnt'] = treesize
        sr_res = {'leaf_node_cnt': treesize}

        for tbm in tbms:
            treepath = tree_files[treesize]
            f_results, s_results = tbm.runBenchmarks(
                treepath, treesize, args.num_reps, args.sig_figs
            )
            sr_res.update(s_results)

            for key in f_results:
                fr_res['benchmark'] = key
                for rtime in f_results[key]:
                    fr_res['time'] = rtime
                    fr_writer.writerow(fr_res)

        sr_writer.writerow(sr_res)

