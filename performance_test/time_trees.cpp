/*
 * Copyright (C) 2016 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * Tests the running times of tree parse/load operations and tree data
 * structure traversals using different implementations of phylogenetic trees.
 * For tree parse/load operations, the minimum time required to parse and load
 * a single tree instance is reported.  For tree traversals, the minimum time
 * required to complete a given number of traversals is reported.
 *
 */

#include <cstddef>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <utility>
#include <stdexcept>
#include <algorithm>
using std::size_t;
using std::string;
using std::cout;
using std::endl;
using std::flush;
using std::cin;
using std::ifstream;
using std::vector;
using std::chrono::steady_clock;
using std::runtime_error;
using std::min_element;

// Define a type alias for a time duration that uses floating-point seconds.
using DurationS = std::chrono::duration<double>;

// Define a type alias for a pair of time floating-point values.
using TimePair = std::pair<double, double>;

#include "lib/cxxopts/cxxopts.hpp"

#include "tree.hpp"
#include "treenode_ptrpair.hpp"
#include "treenode_vector.hpp"
#include "newickparser.hpp"


/**
 * @brief Implements a recursive traversal of a tree data structure.
 */
template <typename NodeType>
void traverseTree(NodeType &treenode) {
    for (size_t cnt = 0; cnt < treenode.getChildCnt(); ++cnt) {
        traverseTree(treenode.getChild(cnt));
    }
}

/**
 * @brief Calculates the times required to parse/load and traverse a tree.
 * 
 * Calculates the time required to parse the given tree file and create an
 * instance of a tree data structure, then calculates the time required to
 * complete a given number of tree traversals.
 * 
 * @param treefile The tree file to parse.
 * @param numtraversals The number of tree traversals.
 * @return TimePair The running times, where pair.first = load/parse time and
 * pair.second = traversal time.
 */
template <typename TreeType>
TimePair timeLoadAndTraversal(const string &treefile, unsigned numtraversals) {
    TimePair results;
    
    // Time tree parse/load time.
    auto start_time = steady_clock::now();
    
    ifstream ifs(treefile);
    if (!ifs) {
        throw runtime_error("Could not open the input tree file \"" + treefile + "\".\n");
    }

    NewickParser<TreeType> parser(ifs);
    auto tree = parser.getNextTree();
    
    auto end_time = steady_clock::now();
    results.first = DurationS(end_time - start_time).count();

    // Time tree traversals.
    start_time = steady_clock::now();    
    
    for (unsigned cnt = 0; cnt < numtraversals; ++cnt) {
        traverseTree(tree.getRoot());
    }
    
    end_time = steady_clock::now();
    results.second = DurationS(end_time - start_time).count();
    
    return results;
}


int main(int argc, char *argv[]) {
    cxxopts::Options opts("time_trees",
                          " Tests the running time performance of phylogenetic tree implementations.");
    bool verbose = false;
    opts.add_options()
    ("h,help", "Print usage information.")
    ("n,numtests", "The number of repeat tree operations to test (default = 1).",
     cxxopts::value<unsigned>()->default_value("1"))
    ("c,numtraversals", "The number of tree traversals per test (default = 1).",
     cxxopts::value<unsigned>()->default_value("1"))
    ("t,treetype", "The tree data structure to use.  Valid values are \"TreeNodePtrPair\" and \"TreeNodeVector\".",
     cxxopts::value<string>()->default_value("TreeNodePtrPair"))
    ("v,verbose", "Generate verbose output.", cxxopts::value<bool>(verbose))
    ("f,treefile", "A Newick-format tree file.",
     cxxopts::value<vector<string>>())
    ;

    opts.parse_positional("treefile");
    opts.parse(argc, argv);

    if (opts.count("help")) {
        cout << opts.help() << endl;
        return 0;
    }
    
    // Get all positional arguments as a vector of strings and do some simple
    // error checking.
    auto &pargs = opts["treefile"].as<vector<string>>();
    if (pargs.size() == 0) {
        cout << "You must provide the name of a Newick-format tree file.\n" << endl;
        return 1;
    } else if (pargs.size() > 1) {
        cout << "Only one input tree file name can be specified.\n" << endl;
        return 1;
    }
    
    const unsigned numtests = opts["numtests"].as<unsigned>();
    const unsigned numtraversals = opts["numtraversals"].as<unsigned>();
    const string treetypename = opts["treetype"].as<string>();
    const string treefile = pargs[0];

    vector<double> loadtimes;
    vector<double> traversetimes;
    TimePair times;
    if (treetypename == "TreeNodePtrPair") {
        for (unsigned cnt = 0; cnt < numtests; ++cnt) {
            times = timeLoadAndTraversal<Tree<TreeNodePtrPair>>(treefile, numtraversals);
            loadtimes.push_back(times.first);
            traversetimes.push_back(times.second);
        }
    } else if (treetypename == "TreeNodeVector") {
        for (unsigned cnt = 0; cnt < numtests; ++cnt) {
            times = timeLoadAndTraversal<Tree<TreeNodeVector>>(treefile, numtraversals);
            loadtimes.push_back(times.first);
            traversetimes.push_back(times.second);
        }
    } else {
        cout << "Unrecognized tree data structure type.\n" << endl;
        return 1;
    }
    
    double min_loadtime = *min_element(loadtimes.cbegin(), loadtimes.cend());
    double min_travtime = *min_element(traversetimes.cbegin(), traversetimes.cend());
    
    if (verbose) {
        cout << "\nTree data structure type: " << treetypename << endl;
        cout << "Minimum parse/load time (n=" << numtests << "): " <<
             min_loadtime << " seconds" << endl;
        cout << "Minimum time for " << numtraversals << " traversals (n=" <<
             numtests << "): " << min_travtime << " seconds\n" << endl;
    } else {
        cout << min_loadtime << " " << min_travtime << endl;
    }
    
    return 0;
}
