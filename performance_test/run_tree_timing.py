#!/usr/bin/python3

import subprocess
import sys


# Define a table of test programs and data structures to test.  Each test
# program must implement the same command-line interface.  Run any of the test
# programs with the "-h" option for details of the required interface.
testprogs = {
#    '../../build/test/perf_test/time_trees': ['TreeNodePtrPair', 'TreeNodeVector'],
    './time_trees-python.py': ['fastpcm', 'Dendropy'],
#    './time_trees-R.r': ['ape', 'apelapply', 'apebyrow']
}

# Define a table of test tree files, where the keys are the numbers of leaf
# nodes in each tree.
tfiles = {
    1000: '../test_data/rtree-1k_taxa.new',
    2000: '../test_data/rtree-1k_taxa.new',
    3000: '../test_data/rtree-1k_taxa.new',
    10000: '../test_data/rtree-10k_taxa.new',
    100000: '../test_data/rtree-100k_taxa.new',
    1000000: '../test_data/rtree-1mil_taxa.new',
    10000000: '../test_data/rtree-10mil_taxa.new',
}

# Define the test cases.  The number of traversals is defined according to the
# number of leaf nodes in the test tree, using the formula
# log10(numtraversals * 2.5) = 6 - log10(num_leaf_nodes).
treesizes =      [1000, 2000, 3000, 10000, 100000, 1000000, 10000000]
numtraversals =  [400,  400, 400, 40,    4,      1,       1]

# The total number of tests to run for each test case.
numtests = 2

# Define exclusions.  If the tree size (defined as the number of leaf nodes)
# equals or exceeds the value set for the given tree data structure name, no
# tests will be run.
exclusions = {
    # Recursive traversals of a tree with 100,000 or more leaf nodes in R was
    # so slow as to be impractical.  A single traversal took more than 500
    # seconds (that is, more than 8 minutes).
    'ape': 100000,
    'apelapply': 100000,
    # Trying to load 1 tree with 10,000,000 leaf nodes in R quickly consumed
    # more than 6 GB of RAM.
    'apebyrow': 10000000
}

# Print the tab-delimited results table header.
print('no. leaf nodes\tno. traversals', end='')
colnames = []
for testprog in sorted(testprogs.keys()):
    for testds in testprogs[testprog]:
        cmd = '{0} -t {1} -d'.format(testprog, testds)
        completed = subprocess.run(
            cmd, shell=True, stdout=subprocess.PIPE, timeout=None
        )
        colnames.extend(completed.stdout.decode('utf-8').strip().split('\t'))
print('\t'.join(colnames))

# Run each test case and print the results.
for treesize, numt in zip(treesizes, numtraversals):
    print(str(treesize) + '\t' + str(numt), end='')
    sys.stdout.flush()

    for testprog in sorted(testprogs.keys()):
        for testds in testprogs[testprog]:
            cmd = '{0} -t {1} -n {2} -c {3} -p {4} {5}'.format(
                testprog, testds, numtests, numt, treesize, tfiles[treesize]
            )
            #print(cmd)
            completed = subprocess.run(
                cmd, shell=True, stdout=subprocess.PIPE, timeout=None
            )
            results = completed.stdout.decode('utf-8').strip()
            print('\t{0}'.format(results), end='')
            sys.stdout.flush()

    print()

