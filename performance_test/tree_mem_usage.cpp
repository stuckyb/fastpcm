/*
 * Copyright (C) 2016 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * This program is designed for testing the memory efficiency of tree data
 * structures.  Given the name of a tree data structure type, an input
 * Newick-format tree file, and a specified number of trees, it loads the
 * requested number of trees into memory, then terminates after the user
 * presses enter.  This allows process memory usage to be tracked using ps or
 * other utilities.
 *
 */

#include <cstddef>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
using std::size_t;
using std::string;
using std::cout;
using std::endl;
using std::flush;
using std::cin;
using std::ifstream;
using std::vector;

#include "lib/cxxopts/cxxopts.hpp"

#include "tree.hpp"
#include "treenode_ptrpair.hpp"
#include "newickparser.hpp"


/**
 * Loads instances of a specified tree data structure (provided as the single
 * template parameter) into memory and waits for user input before deleting the
 * tree instances.
 * 
 * @param treefile A Newick-format tree file.
 * @param numtrees The number of instances to load.
 * @return 0 on success, 1 on failure.
 */
template <typename TreeType>
int loadTreesAndWait(const string &treefile, size_t numtrees) {
    NewickParser<TreeType> parser;
    
    // Use a dynamic array to hold the trees so that the storage overhead is
    // the same per tree, regardless of the number of trees.
    TreeType *treearray = new TreeType[numtrees];
    
    // Create the desired number of tree instances from the input tree file.
    for (size_t cnt = 0; cnt < numtrees; ++cnt) {
        cout << "Loading tree " << (cnt + 1) << "/" << numtrees << "..." << flush;
        ifstream ifs(treefile);
        if (!ifs) {
            cout << "Could not open the input tree file \"" << treefile << "\".\n" << endl;
            return 1;
        }
        parser.setInput(ifs);
        treearray[cnt] = parser.getNextTree();
        cout << " done." << endl;
    }
    
    cout << "Press enter to terminate the program." << endl;
    cin.ignore();
    
    delete [] treearray;
    
    return 0;
}


int main(int argc, char *argv[]) {
    cxxopts::Options opts("tree_mem_usage",
                          " A utility for testing the memory usage of tree data structures.");
    opts.add_options()
    ("h,help", "Print usage information.")
    ("n,numtrees", "The number of tree instances to load.",
     cxxopts::value<size_t>()->default_value("1"))
    ("t,treetype", "The tree data structure to use.",
     cxxopts::value<string>()->default_value("TreeNodePtrPair"))
    ("f,treefile", "A Newick-format tree file.",
     cxxopts::value<vector<string>>())
    ;

    opts.parse_positional("treefile");
    opts.parse(argc, argv);

    if (opts.count("help")) {
        cout << opts.help() << endl;
        return 0;
    }
    
    // Get all positional arguments as a vector of strings and do some simple
    // error checking.
    auto &pargs = opts["treefile"].as<vector<string>>();
    if (pargs.size() == 0) {
        cout << "You must provide the name of a Newick-format tree file.\n" << endl;
        return 1;
    } else if (pargs.size() > 1) {
        cout << "Only one input tree file name can be specified.\n" << endl;
        return 1;
    }
    
    const int numtrees = opts["numtrees"].as<size_t>();
    const string treetypename = opts["treetype"].as<string>();
    const string treefile = pargs[0];

    int res = 1;
    if (treetypename == "TreeNodePtrPair") {
        res = loadTreesAndWait<Tree<TreeNodePtrPair>>(treefile, numtrees);
    } else {
        cout << "Unrecognized tree data structure type.\n" << endl;
    }

    return res;
}
