#!/usr/bin/python3

import os.path  
import re
from pympler import asizeof
from argparse import ArgumentParser


def calcTreeDataStructSize(treesize, library):
    """
    Estimates the size of a tree data structure using known information about
    the data structure implementation.  Currently, this is only implemented for
    fastpcm trees.
    """
    nodecnt = treesize * 2 - 1

    if library == 'fastpcm':
        # Each node includes 1 string (32 bytes), 1 double (8 bytes), 1 size_t
        # (8 bytes), 2 unique_ptrs and 1 raw pointer (24 bytes), for a total of
        # 72 bytes per node.  Assuming all node names are 16 characters or
        # less, the strings will not require more than the initial allocation
        # of 32 bytes each.
        ds_size = nodecnt * 72

        # An additional 56 bytes for the size of the Python object, as
        # determined by sys.getsizeof().
        ds_size += 56
    else:
        ds_size = 0

    return ds_size

argp = ArgumentParser(
    description='Tests the memory footprint of phylogenetic tree '
    'implementations in Python.'
)
argp.add_argument(
    '-m', '--mode', type=str, required=False, default='wait',
    help='The execution mode.  The default, "wait", means wait for terminal '
    'input before exiting, "data" means print data about tree size to the '
    'console and exit.  "wait" mode is intended for empirical study of process '
    'memory allocation.'
)
argp.add_argument(
    '-n', '--numtrees', type=int, required=False, default=1,
    help='The number of tree instances to load (default = 1).'
)
argp.add_argument(
    '-l', '--library', type=str, required=False, default='fastpcm',
    help='The tree library to use.  Valid options are "fastpcm", "dendropy", '
    '"biopython", or "ete" (default = "fastpcm").'
)
argp.add_argument('treefile', help='A Newick-format tree file.')
args = argp.parse_args()

library = args.library

if library == 'fastpcm':
    import fastpcm as plib
elif library == 'dendropy':
    import dendropy as plib
elif library == 'biopython':
    from Bio import Phylo as plib
elif library == 'ete':
    import ete3 as plib
else:
    exit(
        '\nERROR: Unrecognized phylogenetic library: "{0}".\n'.format(
            library
        )
    )

if not(os.path.exists(args.treefile)):
    exit(
        '\nERROR: the tree file "{0}" could not be found.\n'.format(
            args.treefile
        )
    )

numtrees = args.numtrees
treelist = []

for cnt in range(numtrees):
    if args.mode != 'data':
        print(
            'Loading tree {0} of {1}...'.format(cnt + 1, numtrees),
            end='', flush=True
        )

    if library == 'fastpcm':
        treelist.append(plib.Tree.fromFile(args.treefile))
    elif library == 'dendropy':
        treelist.append(plib.Tree.get(path=args.treefile, schema='newick'))
    elif library == 'biopython':
        treelist.append(plib.read(args.treefile, 'newick'))
    elif library == 'ete':
        treelist.append(plib.Tree(args.treefile))

    if args.mode != 'data':
        print(' done.')

if args.mode == 'data':
    # Extract the tree size from the file name.
    treesize = int(
        re.sub('.*rtree-', '', args.treefile).replace('_tips.new', '')
    )

    print(
        '{{"observed": {0}, "expected": {1}}}'.format(
            asizeof.asizeof(treelist[0]),
            calcTreeDataStructSize(treesize, library)
        )
    )
else:
    input('Press enter to terminate the program.\n')

