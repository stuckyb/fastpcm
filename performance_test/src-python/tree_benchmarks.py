"""
Tests the running times of tree parse/load operations and tree data structure
traversals using different implementations of phylogenetic trees in Python.
For tree parse/load operations, the minimum time required to parse and load a
single tree instance is reported.  For tree traversals, the minimum time
required to complete a given number of traversals is reported.
"""

import timeit
from abc import ABC, abstractmethod
import tempfile
import fastpcm
import dendropy as dp
from Bio import Phylo
from ete3 import Tree as EteTree


class Timer:
    """
    A simple context manager for timing blocks of code.
    """
    def __enter__(self):
        self.start_time = timeit.default_timer()
        return self

    def __exit__(self, errtype, errvalue, errtraceback):
        self.elapsed = timeit.default_timer() - self.start_time


class TreeBenchmarks(ABC):
    """
    A base class for library-specific tree benchmarking classes.  At a minimum,
    all child classes must implement a loadTree() method.
    """
    libname = ''

    def __init__(self, ):
        self.inst_benchmarks = {}
        self.noinst_benchmarks = {}

        self.includes = set()
        self.excludes = set()

    def updateInstanceBenchmarks(self, benchmarks):
        """
        Add or update benchmarks that require a tree instance.
        """
        self.inst_benchmarks.update(benchmarks)

    def updateNoInstanceBenchmarks(self, benchmarks):
        """
        Add or update benchmarks that do not require a tree instance.
        """
        self.noinst_benchmarks.update(benchmarks)

    def printBenchmarks(self):
        """
        Prints all registered benchmarks.
        """
        print('Benchmarks for {0}:'.format(self.libname))
        print('  load')

        for bmn in sorted(self.inst_benchmarks):
            print('  {0}'.format(bmn))

        for bmn in sorted(self.noinst_benchmarks):
            print('  {0}'.format(bmn))

    def setIncludes(self, benchmarks):
        """
        Specifies benchmark names to run.  All other benchmarks will be
        skipped.

        benchmarks: An interable of benchmark names.
        """
        self.includes.update(benchmarks)

    def setExcludes(self, benchmarks):
        """
        Specifies benchmark names to skip.  All other benchmarks will be run
        unless an include list was also specified.
        """
        self.excludes.update(benchmarks)

    def getColNames(self):
        to_run = self._getRunList()

        colnames = []

        if 'load' in to_run:
            colnames.append(self.libname + '-load')

        for key in sorted(self.inst_benchmarks):
            if key in to_run:
                colnames.append(self.libname + '-' + key)
        for key in sorted(self.noinst_benchmarks):
            if key in to_run:
                colnames.append(self.libname + '-' + key)

        return colnames

    def _getRunList(self):
        """
        Returns a set of benchmarks to run, given any explicit include or
        exclude values.
        """
        # Start with the set of explicit includes.
        to_run = set(self.includes)
        if len(to_run) > 0:
            # See if we have any benchmarks that require a tree instance.
            for benchmark in self.includes:
                if benchmark in self.inst_benchmarks:
                    to_run.add('load')
        else:
            # No explicit includes, so run everything.
            to_run.update(self.inst_benchmarks)
            to_run.update(self.noinst_benchmarks)
            to_run.add('load')

        # Process any excludes.
        for benchmark in self.excludes:
            if benchmark in to_run:
                to_run.remove(benchmark)
    
            if benchmark == 'load':
                # We need to exclude all benchmarks that require a tree
                # instance.
                for sub_benchmark in self.inst_benchmarks:
                    if sub_benchmark in to_run:
                        to_run.remove(sub_benchmark)

        return to_run

    def runBenchmarks(self, treepath, treesize, reps=1, sigfigs=3):
        """
        Runs all benchmarks for the specified number of repititions and returns
        a tuple containing
        (running times for every trial, the minimum time for each test).
        """
        to_run = self._getRunList()
        
        rtimes = {}

        for rep in range(reps):
            print(
                'Running all {0} benchmarks for tree size {1:,}, repetition '
                '{2} of {3}...'.format(self.libname, treesize, rep + 1, reps)
            )
            new_times = self.runInstanceBenchmarks(to_run, treepath, treesize)
            new_times.update(self.runNoInstanceBenchmarks(to_run, treesize))

            for key in new_times:
                if key not in rtimes:
                    rtimes[key] = []
                rtimes[key].append(new_times[key])

        if sigfigs > 0:
            numformat = '{0:.' + str(sigfigs) + 'g}'
        else:
            numformat = '{0:g}'

        summary = {}
        for key in rtimes:
            mintime = min(rtimes[key])

            # Round floating-point times to the required number of significant
            # figures.
            if isinstance(mintime, float):
                summary[key] = numformat.format(mintime)
            else:
                summary[key] = str(mintime)

        return (rtimes, summary)

    def runInstanceBenchmarks(self, to_run, treepath, treesize):
        """
        Run all benchmarks that require a tree instance.
        """
        results = {}

        if 'load' in to_run:
            print('  Running benchmark "load"...')
            tree, rtime = self.loadTree(treepath, treesize)
            if rtime is not None:
                results[self.libname + '-load'] = rtime
            else:
                results[self.libname + '-load'] = 'N/A'
        else:
            tree = None
        
        for key in sorted(self.inst_benchmarks):
            if key in to_run:
                colname = self.libname + '-' + key

                if tree is not None:
                    print('  Running benchmark "{0}"...'.format(key))
                    rtime = self.inst_benchmarks[key](tree, treesize)
                else:
                    rtime = None

                if rtime is not None:
                    results[colname] = rtime
                else:
                    results[colname] = 'N/A'

        return results

    def runNoInstanceBenchmarks(self, to_run, treesize):
        """
        Run all benchmarks that do not require a tree instance.
        """
        results = {}

        for key in sorted(self.noinst_benchmarks):
            if key in to_run:
                colname = self.libname + '-' + key

                print('  Running benchmark "{0}"...'.format(key))
                rtime = self.noinst_benchmarks[key](treesize)

                if rtime is not None:
                    results[colname] = rtime
                else:
                    results[colname] = 'N/A'

        return results

    @abstractmethod
    def loadTree(self, treepath, treesize):
        return (None, None)


class FastpcmBenchmarks(TreeBenchmarks):
    libname = 'fastpcm'

    def __init__(self):
        super().__init__()

        self.updateInstanceBenchmarks({
            'save': self.saveTree,
            'traverse_recursive': self.recursiveTraverse,
            'age': self.treeAge
        })

        self.updateNoInstanceBenchmarks({
            'rand_birth_death': self.birthDeathTree
        })

    def loadTree(self, treepath, treesize):
        if treesize > 100000000:
            return (None, None)

        with Timer() as timer:
            tree = fastpcm.Tree.fromFile(treepath)

        return (tree, timer.elapsed)

    def saveTree(self, tree, treesize):
        with tempfile.TemporaryFile(mode='w') as fp:
            with Timer() as timer:
                tree.write(fp, fastpcm.OutputFormats.NEWICK)

        return timer.elapsed

    def recursiveTraverse(self, tree, treesize):
        with Timer() as timer:
            self._recursiveTraverse(tree.getRoot())

        return timer.elapsed

    def _recursiveTraverse(self, treenode):
        """
        Implements a recursive traversal of a fastpcm tree.
        """
        for cnt in range(treenode.getChildCnt()):
            self._recursiveTraverse(treenode.getChild(cnt))

    def treeAge(self, tree, treesize):
        with Timer() as timer:
            tree.depth()

        return timer.elapsed

    def birthDeathTree(self, treesize):
        if treesize > 100000000:
            return None

        with Timer() as timer:
            rtree = fastpcm.random.birthDeathTree(
                birth_rate=1.0, death_rate=0.0, final_tipcnt=treesize
            )

        return timer.elapsed


class DendropyBenchmarks(TreeBenchmarks):
    libname = 'Dendropy'

    def __init__(self):
        super().__init__()

        self.updateInstanceBenchmarks({
            'save': self.saveTree,
            'traverse_recursive': self.recursiveTraverse,
            'traverse_iter': self.iteratorTraverse,
            'age': self.treeAge
        })

        self.updateNoInstanceBenchmarks({
            'rand_birth_death': self.birthDeathTree
        })

    def loadTree(self, treepath, treesize):
        # Trying to load even 1 tree with 10,000,000 leaf nodes in Dendropy
        # used up more than 4 GB of RAM.
        if treesize >= 10000000:
            return (None, None)

        with Timer() as timer:
            tree = dp.Tree.get(path=treepath, schema='newick')

        return (tree, timer.elapsed)

    def saveTree(self, tree, treesize):
        with tempfile.TemporaryFile(mode='w') as fp:
            with Timer() as timer:
                tree.write(file=fp, schema='newick')

        return timer.elapsed

    def treeAge(self, tree, treesize):
        with Timer() as timer:
            tree.seed_node.distance_from_tip()

        return timer.elapsed

    def recursiveTraverse(self, tree, treesize):
        with Timer() as timer:
            self._recursiveTraverse(tree.seed_node)

        return timer.elapsed

    def _recursiveTraverse(self, treenode):
        """
        Implements a recursive traversal of a Dendropy tree.
        """
        for child in treenode.child_nodes():
            self._recursiveTraverse(child)

    def iteratorTraverse(self, tree, treesize):
        """
        Traverses a Dendropy tree using an iterator.
        """
        with Timer() as timer:
            for node in tree.preorder_node_iter():
                pass

        return timer.elapsed

    def birthDeathTree(self, treesize):
        if treesize > 6000:
            return None

        with Timer() as timer:
            rtree = dp.simulate.treesim.birth_death_tree(
                1.0, 0.0, num_extant_tips=treesize
            )

        return timer.elapsed


class BioPythonBenchmarks(TreeBenchmarks):
    libname = 'BioPython'

    def __init__(self):
        super().__init__()

        self.updateInstanceBenchmarks({
            'save': self.saveTree,
            'traverse': self.traverse
        })

        self.updateNoInstanceBenchmarks({
        })

    def loadTree(self, treepath, treesize):
        # Trying to load even 1 tree with 10,000,000 leaf nodes in BioPython
        # used up more than 6 GB of RAM.
        if treesize >= 10000000:
            return (None, None)

        with Timer() as timer:
            tree = Phylo.read(treepath, 'newick')

        return (tree, timer.elapsed)

    def saveTree(self, tree, treesize):
        with tempfile.TemporaryFile(mode='w') as fp:
            with Timer() as timer:
                Phylo.write(tree, fp, 'newick')

        return timer.elapsed

    def traverse(self, tree, treesize):
        with Timer() as timer:
            self._traverse(tree.root)

        return timer.elapsed

    def _traverse(self, clade):
        """
        Implements a recursive traversal of a BioPython tree.
        """
        for child in clade.clades:
            self._traverse(child)


class ETEBenchmarks(TreeBenchmarks):
    libname = 'ETE'

    def __init__(self):
        super().__init__()

        self.updateInstanceBenchmarks({
            #'save': self.saveTree,
            'traverse_recursive': self.recursiveTraverse,
            'traverse_traverse': self.traverseTraverse,
            'age': self.treeAge
        })

        self.updateNoInstanceBenchmarks({
        })

    def loadTree(self, treepath, treesize):
        # Trying to load even 1 tree with 10,000,000 leaf nodes in ETE used up
        # more than 4 GB of RAM.
        if treesize >= 10000000:
            return (None, None)

        with Timer() as timer:
            tree = EteTree(treepath)

        return (tree, timer.elapsed)

    def saveTree(self, tree, treesize):
        # ETE's write() doesn't work with a file-like object (a file path is
        # required), so this is disabled for now.
        with tempfile.TemporaryFile(mode='w') as fp:
            with Timer() as timer:
                tree.write(format=0, outfile=fp)

        return timer.elapsed

    def treeAge(self, tree, treesize):
        with Timer() as timer:
            tree.get_farthest_leaf()

        return timer.elapsed

    def recursiveTraverse(self, tree, treesize):
        with Timer() as timer:
            self._recursiveTraverse(tree)

        return timer.elapsed

    def _recursiveTraverse(self, tree):
        """
        Implements a recursive traversal of an ETE tree.
        """
        for child in tree.children:
            self._recursiveTraverse(child)

    def traverseTraverse(self, tree, treesize):
        """
        Traverses an ETE tree using the traverse() method.
        """
        with Timer() as timer:
            for node in tree.traverse('preorder'):
                pass

        return timer.elapsed


def getBenchmarkers(libnames=['all']):
    """
    Returns instances of 1 or more benchmarking classes, according to the
    string values provided in "libnames", which should be a list of tree
    library names to test.  The names are not case-sensitive.  If any element
    of libnames is "all", instances of all benchmarking classes will be
    returned.
    """
    b_classes = [
        FastpcmBenchmarks, DendropyBenchmarks, BioPythonBenchmarks,
        ETEBenchmarks
    ]

    b_instances = []
    loaded = set()

    for libname in libnames:
        for b_class in b_classes:
            l_libname = libname.lower()
            if l_libname == b_class.libname.lower() or l_libname == 'all':
                if b_class.libname not in loaded:
                    b_instances.append(b_class())
                    loaded.add(b_class.libname)

    return b_instances

