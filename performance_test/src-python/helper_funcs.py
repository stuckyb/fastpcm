
# Helper functions for running performance testing benchmarks.

import os.path


def getTreeFiles(treesizes, output_dir='test_data'):
    """
    Generates a map of tree sizes to tree file paths, and creates any missing
    test tree files.
    """
    import fastpcm

    checkOutputDir(output_dir)

    tfiles_map = {}

    # Generate the tree file paths and create any missing tree files.
    for treesize in treesizes:
        fpath = os.path.join(output_dir, 'rtree-{0}_tips.new'.format(treesize))
        tfiles_map[treesize] = fpath

        if not(os.path.exists(fpath)):
            print(
                'Generating random tree with {0:,} leaf nodes...'.format(
                    treesize
                )
            )
            rtree = fastpcm.random.birthDeathTree(
                birth_rate=1.0, death_rate=0.0, final_tipcnt=treesize
            )
            rtree.write(fpath, fastpcm.OutputFormats.NEWICK)

    return tfiles_map

def checkOutputDir(output_dir='results'):
    """
    Verifies that an output directory exists, and if not, creates it.
    """
    if os.path.isfile(output_dir):
        exit(
            '\nA file with the same name as a required directory, "{0}", '
            'already exists.\n'.format(output_dir)
        )

    if not(os.path.isdir(output_dir)):
        os.mkdir(output_dir)

