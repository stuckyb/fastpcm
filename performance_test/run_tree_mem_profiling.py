#!/usr/bin/python3
"""
Automates profiling the memory footprint of different tree data structures.
For details of the methods used, see tree_mem_info.txt.
"""

import sys
import math
import csv
import pexpect
import subprocess
import os.path
import json
from argparse import ArgumentParser

scriptdir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(scriptdir, 'src-python'))
from helper_funcs import getTreeFiles, checkOutputDir


def getOutputPaths(basename, output_dir='results'):
    """
    Prepares the results file paths, and returns the tuple
    (process_results_path, datastructure_results_path).
    """
    checkOutputDir(output_dir)

    pr_path = os.path.join(output_dir, basename + '-process.csv')
    dsr_path = os.path.join(output_dir, basename + '-data_structure.csv')

    if os.path.exists(pr_path):
        exit('\nThe output file {0} already exists.\n'.format(pr_path))

    if os.path.exists(dsr_path):
        exit('\nThe output file {0} already exists.\n'.format(dsr_path))

    return (pr_path, dsr_path)

def getMemFootprint(testprog, treefile, numtrees, library):
    """
    Returns the memory footprint of a tree data structure test process after
    the test process has loaded all requested tree instances.  The memory
    footprint is the "virtual set size" as returned by the "ps" utility.
    """
    tproc = pexpect.spawn(
        testprog, ['-l', library, '-n', str(numtrees), treefile], timeout=None
    )
    
    # Wait until all tree instances have been loaded.
    tproc.expect_exact('Press enter to terminate the program.')
    
    # Get the "virtual set size" of the running test process.
    res = pexpect.run('ps --no-headers ww -p ' + str(tproc.pid) + ' -o vsz')
    memuse = int(res.strip())
    
    # Terminate the test process.
    tproc.sendline()
    tproc.wait()

    return memuse

def getTreeSizeData(testprog, treefile, library):
    """
    Runs a tree test process in "data" mode, which obtains observed and
    estimated tree data structure sizes.
    """
    args = [testprog, '-l', library, '-n', '1', '-m', 'data', treefile]
    res = subprocess.run(args, stdout=subprocess.PIPE)

    json_str = res.stdout.decode('utf-8')

    return json.loads(json_str)

def getTreeCounts(treesize):
    """
    Calculates the different numbers of trees to test for a given tree size
    (defined as the number of leaf nodes).  The maximum number of trees for a
    given tree size is defined as:
    log10(numtrees) = 7 - log10(num_leaf_nodes).
    """
    # Determine the maximum number of trees for the given tree size.
    numt_max = int(round(10 ** (7 - math.log10(treesize))))

    # Determine the numbers of trees to load for the given tree size.
    t_numtrees = []
    exp = 0
    while 10 ** exp < numt_max:
        t_numtrees.append(10 ** exp)
        exp += 1

    t_numtrees.append(numt_max)

    return t_numtrees

def getLibraryDataStructSizes(testprog, library, writer):
    row = {}
    row['library'] = library

    print(
        'Getting tree data structure size information for '
        '"{0}"...'.format(library)
    )
    print('  Testing tree size: ', end='')

    for treesize in treesizes:
        row['leaf_node_cnt'] = treesize

        print('{0:,}... '.format(treesize), end='', flush=True)

        if treesize <= tsize_limits[library]:
            res = getTreeSizeData(testprog, tree_files[treesize], library)

            row['size_KiB-observed'] = res['observed'] / 1024
            row['size_MiB-observed'] = res['observed'] / (1024 * 1024)
            row['node_size_B-observed'] = res['observed'] / (treesize * 2 - 1)

            row['size_KiB-expected'] = res['expected'] / 1024
            row['size_MiB-expected'] = res['expected'] / (1024 * 1024)
            row['node_size_B-expected'] = res['expected'] / (treesize * 2 - 1)
        else:
            row['size_KiB-observed'] = 'N/A'
            row['size_MiB-observed'] = 'N/A'
            row['node_size_B-observed'] = 'N/A'

            row['size_KiB-expected'] = 'N/A'
            row['size_MiB-expected'] = 'N/A'
            row['node_size_B-expected'] = 'N/A'

        writer.writerow(row)

    print('done.')

def testLibraryMemUsage(testprog, library, writer):
    """
    Runs empirical memory usage testing for a given phylogenetic tree library
    across all test cases and writes the results to a csv.DictWriter instance.
    """
    row = {}

    # Get the base memory usage.
    base_size = getMemFootprint(testprog, tree_files[1000], 0, library)
    print(
        'Base memory footprint (KiB) for {0}: {1}'.format(
            library, base_size
        )
    )
    row['base_size_KiB'] = base_size

    for treesize in treesizes:
        t_numtrees = getTreeCounts(treesize)

        # Run each test case and print a table of results.
        print('no. leaf nodes\tno. trees\tsize (KiB)')
        for t_numt in t_numtrees:
            row['leaf_node_cnt'] = treesize
            row['tree_cnt'] = t_numt

            if treesize <= tsize_limits[library]:
                memuse = getMemFootprint(
                    testprog, tree_files[treesize], t_numt, library
                )
                t_memuse = (memuse - base_size) / t_numt
                print('{0}\t\t{1}\t\t{2}'.format(treesize, t_numt, memuse))
                row['raw_size_KiB'] = memuse
                row['tree_size_MiB'] = t_memuse / 1024
                row['node_size_B'] = (t_memuse * 1024) / (treesize * 2 - 1)
            else:
                row['raw_size_KiB'] = 'N/A'
                row['tree_size_MiB'] = 'N/A'
                row['node_size_B'] = 'N/A'
            
            writer.writerow(row)


argp = ArgumentParser(
    description='Runs memory usage profiling for phylogenetic tree '
    'implementations.'
)
argp.add_argument(
    '-o', '--output', type=str, required=False,
    default='tree_mem_usage', help='The base name of the output CSV file '
    '(default: "tree_mem_usage").'
)
argp.add_argument(
    '-l', '--libraries', type=str, required=False, default='',
    help='A comma-separated list of the phylogenetic libraries to test.  The '
    'default is to test everything.'
)
args = argp.parse_args()

testprogs = [
    (
        os.path.join(scriptdir, 'src-python/tree_mem_profiling.py'),
        ['fastpcm', 'dendropy', 'biopython', 'ete']
    ),
    (
        os.path.join(scriptdir, 'src-r/tree_mem_profiling.r'),
        ['ape']
    )
]

if args.libraries == '':
    libs_to_test = []
    for testprog in testprogs:
        libs_to_test.extend(testprog[1])
else:
    libs_to_test = args.libraries.split(',')

pr_path, dsr_path = getOutputPaths(args.output)

treesizes = [1000, 2000, 4000, 10000, 100000, 1000000, 10000000]
tree_files = getTreeFiles(treesizes)

# Define tree size limits for each library.  Each number is the maximum tree
# size that will be analyzed for each library.
tsize_limits = {
    'fastpcm': 10000000,
    'dendropy': 1000000,
    'biopython': 1000000,
    'ete': 1000000,
    'ape': 1000000
}

with open(pr_path, 'w') as pr_out, open(dsr_path, 'w') as dsr_out:
    dsr_writer = csv.DictWriter(
        dsr_out, [
            'leaf_node_cnt', 'library', 'size_KiB-observed',
            'size_MiB-observed', 'node_size_B-observed', 'size_KiB-expected',
            'size_MiB-expected', 'node_size_B-expected'
        ]
    )
    dsr_writer.writeheader()

    pr_writer = csv.DictWriter(
        pr_out, [
            'leaf_node_cnt', 'tree_cnt', 'library', 'base_size_KiB',
            'raw_size_KiB', 'tree_size_MiB', 'node_size_B'
        ]
    )
    pr_writer.writeheader()

    for testprog  in testprogs:
        for library in testprog[1]:
            if library in libs_to_test:
                getLibraryDataStructSizes(testprog[0], library, dsr_writer)
                testLibraryMemUsage(testprog[0], library, pr_writer)

