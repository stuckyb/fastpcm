#!/usr/bin/Rscript

# Tests the running times of tree parse/load operations and tree data structure
# traversals using implementations of phylogenetic trees in R.  For tree
# parse/load operations, the minimum time required to parse and load a single
# tree instance is reported.  For tree traversals, the minimum time required to
# complete a given number of traversals is reported.

library('ape')
library('optparse')


# Implements a recursive traversal of an ape tree using a for loop.
traverseApeNode = function(nodenum, tree) {
    childnode_indices = which(tree$edge[,1] == nodenum)
    childnode_nums = tree$edge[childnode_indices,2]
    for (childnode_num in childnode_nums) {
        traverseApeNode(childnode_num, tree)
    }
}

# Initiates a recursive traversal of an ape tree using a for loop.
traverseApeTree = function(tree) {
    rootnode = length(tree$tip.label) + 1
    traverseApeNode(rootnode, tree)
}

# Implements a recursive traversal of an ape tree using lapply.
traverseApeNodeLapply = function(nodenum, tree) {
    childnode_indices = which(tree$edge[,1] == nodenum)
    childnode_nums = tree$edge[childnode_indices,2]

    lapply(childnode_nums, traverseApeNodeLapply, tree)
}

# Initiates a recursive traversal of an ape tree using lapply.
traverseApeTreeLapply = function(tree) {
    rootnode = length(tree$tip.label) + 1
    traverseApeNodeLapply(rootnode, tree)
}

# A stub function for the simplified preorder traversal.
processNode = function(node) {
}

# Implements a simplified preorder traversal by simply walking row by row
# through the edge matrix.  In practice, this would be of only very limited
# usefulness and does not allow for complex tree analyses.
traverseApeTreeByRow = function(tree) {
    apply(tree$edge, 1, processNode)
}


optslist = list(
    make_option(c('-n','--numtests'), type='integer', default=1,
                help='The number of repeat tree tree operations to test (default = 1).'),
    make_option(c('-c','--numtraversals'), type='integer', default=1,
	        help='The number of tree traversals per test (default = 1).'),
    make_option(c('-t','--treetype'), type='character', default='ape',
		help=paste('The tree data structure to use. ',
                           'Valid options are "ape", "apelapply", and "apebyrow" (default = "ape").')),
    make_option(c('-v','--verbose'), type='logical', action='store_true',
		default=FALSE, help='Generate verbose output.')
)

argp = OptionParser(usage='%prog [options] treefile', option_list=optslist,
                    description='Tests the running time performance of phylogenetic tree implementations in R.')
args = parse_args(argp, positional_arguments=1)

numtests = args$options$numtests
numtraversals = args$options$numtraversals
treetypename = args$options$treetype
verbose = args$options$verbose
treefile = args$args[1]

loadtimes = rep(NULL, numtests)
traversetimes = rep(NULL, numtests)

if ((treetypename == 'ape') || (treetypename == 'apelapply') || (treetypename == 'apebyrow')) {
    for (cnt in 1:numtests) {
        start = proc.time()
        tree = read.tree(file=treefile)
        elapsed = proc.time() - start
	loadtimes[cnt] = summary(elapsed)[3]

        if (treetypename == 'ape') {
            start = proc.time()
            traverseApeTree(tree)
            elapsed = proc.time() - start
    	    traversetimes[cnt] = summary(elapsed)[3]
        } else if (treetypename == 'apelapply') {
            start = proc.time()
            traverseApeTreeLapply(tree)
            elapsed = proc.time() - start
    	    traversetimes[cnt] = summary(elapsed)[3]
        } else {
            start = proc.time()
            traverseApeTreeByRow(tree)
            elapsed = proc.time() - start
    	    traversetimes[cnt] = summary(elapsed)[3]
        }
    }
} else {
    cat('Error: Unrecognized tree data structure type.\n\n')
    quit()
}

if (verbose) {
    cat('\nTree data structure type:', treetypename, '\n')
    cat('Minimum parse/load time (n=', numtests, '): ', min(loadtimes),
        ' seconds\n', sep='')
    cat('Minimum time for ', numtraversals, ' traversals (n=', numtests, '): ',
        min(traversetimes), ' seconds\n\n', sep='')
} else {
    cat(min(loadtimes), min(traversetimes), '\n')
}

