
## Build instructions

### Building and testing the C++ code

To build the code, you will need [CMake](https://cmake.org/) and the standard C++ build system for your operating system (e.g., make and gcc for Linux, Visual C++ for Windows).  Follow these steps to build the source code:

1. Create a new folder called `build` inside the source directory.
   ```
   $ cd /path/to/source
   $ mkdir build
   ```
2. Enter the new directory and run `cmake`, pointing it to the root of the source directory.
   ```
   $ cd build
   $ cmake ..
   ```
3. This will create a platform-specific build configuration file or project in the `build` directory.  For example, on Linux (or Cygwin, I assume), you can now run `make` to actually build the project.
   ```
   $ make
   ```
   On Windows, you should end up with a Visual C++ project that you can compile (I think).
4. To run the tests, use the build target `test`.  E.g., with `make`, you would run the following.
   ```
   $ make test
   ```
   To see detailed test output, run one of the following.
   ```
   $ make test ARGS=-V
   ```
   ```
   $ctest -V
   ```

### Building, installing and testing the Python package

1. Make `py_package` the current working directory.

2. Build and install the package locally in "development mode".
   ```
   pip install --user -e .
   ```
   Or, to guarantee installing with a particular Python binary (e.g., Python 2 or Python 3):
   ```
   $ python3 -m pip install --user -e .
   ```
   Basically, a "development mode" install means that the installed "package" is really just a link to the package source location.  The advantage of this is that any changes to the package's Python source code will update automatically with no need to rebuild/reinstall.  Changes to the C++ source will of course require a rebuild/reinstall, which means just running the above install command again.  Note that there is no need to run the `build` command described in the previou section -- `pip` will take of building automatically.
   The addition of the `--user` flag means that the package is installed for the current user only, not system-wide.  On Linux, user packages are placed in `~/.local/lib/python3.5` (or whatever Python version you use for the install).

3. To run all unit tests, make `py_package/test` the current working directory, then run
   ```
   $ python run_tests.py
   ```
   This will automatically discover and run all unit tests.  To run a specific test module, run
   ```
   $ python run_tests.py test_module
   ```

4. To uninstall the "development mode" package installation, just use the usual `pip` command, e.g.:
   ```
   $ pip uninstall PACKAGE_NAME
   ```
   or
   ```
   $ python3 -m pip uninstall PACKAGE_NAME
   ```

### Building the Python package without installing (e.g., for distribution)

1. Make `py_package` the current working directory.

2. Run
   ```
   $ python setup.py build
   ```
   to build the full Python package.  The package will be placed in a directory called `./build/lib.SYSTEM-PYVERSION`.

3. To build a "wheel" (binary installation file), run
   ```
   $ python setup.py bdist_wheel
   ```
   The wheel file will be placed in a directory called `dist`.


## Other notes

This is documentation that should eventually be organized elsewhere, but is dumped here for now.

### Python code layout and procedures

The best (i.e., most recent and most official) documentation for packaging and distributing Python projects appears to be the [Python Packaging User Guide](https://packaging.python.org/).  The guide page [Packaging and distributing projects](https://packaging.python.org/guides/distributing-packages-using-setuptools/) is especially relevant here.

Regarding file layout for a distributable python library project, there is a lot of advice on the Web, much of it not particularly good.  The most well-reasoned layout I have seen is [in this post](https://blog.ionelmc.ro/2014/05/25/python-packaging/).  The most critical feature is that the Python packages and code are placed in a separate `src` directory.  This is important for testing, because it ensure that when the package is installed in "development" mode, other stuff, such as `setup.py`, is not also globally importable.

