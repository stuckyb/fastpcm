Description
==================
The goal of **fastpcm** is to develop software that makes it easy for biologists to use phylogenetic comparative methods (PCMs) on very large phylogenetic trees.  (Phylogenetic trees are graphs that depict the evolutionary histories of a group of organisms.)  Broadly speaking, these methods involve statistical analyses of the evolutionary histories of biological traits (e.g., species' coloration patterns, species' feeding strategies, etc.).  This software will be implemented primarily in a relatively low-level, compiled language (currently C++), and language bindings will be implemented for higher-level languages commonly used by biologists (Currently supporting Python).  

Functions
=========

Tree Class Functions
--------------------
*************
.. py:module:: fastpcm

   .. py:class:: TreeNode

      .. py:function:: getName()

         :brief: Gets the name of this TreeNode.
         :rtype: the message id

      .. py:function:: setName(newname)
 
         :param newname: The new name of the TreeNode
         :brief: Sets the name of this TreeNode.
         :rtype: void

      .. py:function:: setName()

         :brief: Gets the branch length from this TreeNode to its parent.
         :rtype: double

      .. py:function:: setBranchLength(branchlen)

         :param branchlen: The new branch length of the TreeNode
         :brief: Sets the branch length from this TreeNode to its parent.
         :rtype: void

      .. py:function:: addChild(name, branchlen)

         :param name: The name of the child node.
         :param branchlen: The length of the branch leading from the new node to its parent.
         :brief: | Adds a new child node to this tree node.
                 |
                 | Creates a new child node of this tree node and returns a reference to the new child node.
         :rtype: TreeNodePtrPair

      .. py:function:: getChildCnt()

         :brief: Returns the number of child nodes of this tree node.
         :rtype: std::size_t

      .. py:function:: getChild(index)

         :param index: The index of the child
         :brief: | Gets the child node at the specified index. 
                 |
                 | Returns a reference to the child node at the specified index.  If index is invalid (i.e., if index >= the total number of child nodes), a range_error exception is thrown.
         :rtype: TreeNodePtrPair

      .. py:function:: getParent()

         :brief: | Gets the parent node of this tree node.
                 |
                 | Returns a reference to the parent node of this tree node.  If this node does not have a parent (i.e., it is the root node of a tree), a range_error exception is thrown.
         :rtype: TreeNodePtrPair

   .. py:class:: Tree

      .. py:function:: fromString(treestr)

         :param treestr: A string containing a Newick tree definition.
         :brief: Creates a new Tree object from a string in Newick format.
         :rtype: Tree

      .. py:function:: fromFile(path)

         :param treestr: The path of Newick-formatted tree file.
         :brief: Creates a new Tree object from a tree file in Newick format.
         :rtype: Tree

      .. py:function:: getRoot()

         :brief: Returns a reference to the root node of this tree.
         :rtype: TreeNodePtrPair

      .. py:function:: print([format=OutputFormats::NEWICK])

         :brief: 
         :rtype:

      .. py:function:: write([os, OutputFormats format=OutputFormats::NEWICK])

         :brief:
         :rtype:

      .. py:function:: toString([OutputFormats format=OutputFormats::NEWICK])

         :brief:
         :rtype: string

   .. py:class:: RandomTreeGenerator

      .. py:function:: birthDeathTree(birth_rate, death_rate, final_tipcnt)

         :param birth_rate:
         :param death_rate:
         :param final_tipcnt:
         :brief: 
         :rtype: 
