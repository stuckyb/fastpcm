# Copyright (C) 2018 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import re
import sys
import platform
import subprocess

from setuptools import setup, Extension, find_packages
from setuptools.command.build_ext import build_ext
from distutils.version import LooseVersion


class CMakeExtension(Extension):
    def __init__(self, name, sourcedir='.'):
        """
        Note that the value of "name" has absolutely no effect on the file name
        of the compiled module file(s); that is controlled entirely by
        CMakeLists.txt.  The value of "name" can, however, determine the
        location of the compiled file(s) if "name" contains a package path.

        name: The extension module name.
        sourcedir: The location of a source tree with a CMakeLists.txt file.
        """
        Extension.__init__(self, name, sources=[])
        self.sourcedir = os.path.abspath(sourcedir)


class CMakeBuild(build_ext):
    def run(self):
        try:
            out = subprocess.check_output(['cmake', '--version'])
        except OSError:
            raise RuntimeError(
                "CMake must be installed to build the following extensions: " +
                ", ".join(e.name for e in self.extensions)
            )

        if platform.system() == "Windows":
            cmake_version = LooseVersion(
                re.search(r'version\s*([\d.]+)', out.decode()).group(1)
            )
            if cmake_version < '3.1.0':
                raise RuntimeError("CMake >= 3.1.0 is required on Windows")

        # Note that each object in self.extensions is an instance of
        # CMakeExtension.
        for ext in self.extensions:
            self.build_extension(ext)

    def build_extension(self, ext):
        # extdir is where the compiled extension module(s) will be placed.
        extdir = os.path.abspath(
            os.path.dirname(self.get_ext_fullpath(ext.name))
        )
        cmake_args = [
            '-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=' + extdir,
            '-DPYTHON_EXECUTABLE=' + sys.executable
        ]

        cfg = 'Debug' if self.debug else 'Release'
        build_args = ['--config', cfg]

        if platform.system() == "Windows":
            cmake_args += [
                '-DCMAKE_LIBRARY_OUTPUT_DIRECTORY_{}={}'.format(cfg.upper(), extdir)
            ]
            if sys.maxsize > 2**32:
                cmake_args += ['-A', 'x64']
            build_args += ['--', '/m']
        else:
            cmake_args += ['-DCMAKE_BUILD_TYPE=' + cfg]
            build_args += ['--', '-j2']

        env = os.environ.copy()
        env['CXXFLAGS'] = '{} -DVERSION_INFO=\\"{}\\"'.format(
            env.get('CXXFLAGS', ''), self.distribution.get_version()
        )
        if not os.path.exists(self.build_temp):
            os.makedirs(self.build_temp)

        # Run cmake to set up the build system.
        subprocess.check_call(
            ['cmake', ext.sourcedir] + cmake_args, cwd=self.build_temp, env=env
        )
        # Run cmake again to do the actual build, using the local (i.e.,
        # OS-specific) build system.
        subprocess.check_call(
            ['cmake', '--build', '.'] + build_args, cwd=self.build_temp
        )


setup(
    name='fastpcm',
    description='Software for fast phylogenetic computing in Python.',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Topic :: Scientific/Engineering :: Bio-Informatics'
    ],
    packages=find_packages('src'),
    # Specify where the root package is located.
    package_dir={'': 'src'},
    # Note that the module name passed to CMakeExtension does absolutely
    # nothing with regards to the name of the compiled module file -- that is
    # entirely controlled by CMakeLists.txt.  The only thing that the module
    # name passed to CmakeExtension does control is *where* the compiled module
    # is located.  In this case, the name "fastpcm/" (which can also be
    # specified as "fastpcm/fastpcm", "fastpcm.", "fastpcm.fastpcm",
    # "fastpcm/nonsense", etc.) specifies that the compiled module will be
    # placed in a subdirectory called "fastpcm".
    ext_modules=[CMakeExtension('fastpcm/', 'src/fastpcm')],
    cmdclass=dict(build_ext=CMakeBuild),
    zip_safe=False
)

