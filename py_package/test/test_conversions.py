import unittest
import os.path
from io import StringIO
from fastpcm import Tree

ttree = '(b:1.2,c:2.4)a:0.6;'

class TestConversions(unittest.TestCase):
    def test_Dendropy(self):
        tree = Tree.fromString(ttree)
        dendroTree=tree.toDendropy()
        self.assertEqual(ttree+"\n", dendroTree.as_string(schema="newick"))

    def test_ETE(self):
        tree = Tree.fromString(ttree)
        eteTree = tree.toETE()
        self.assertEqual(ttree,eteTree.write(format=1, format_root_node=True))
