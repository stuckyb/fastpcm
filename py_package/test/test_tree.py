# Copyright (C) 2018 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest
import os.path
from io import StringIO
from fastpcm import Tree, OutputFormats


# Define a simple test tree in Newick format.  This is a binary tree with a
# root node ('a') and two child nodes ('b' and 'c').
ttree = '(b:1.2,c:2.4)a:0.6;'

class TestTree(unittest.TestCase):
    def _checkTestTree(self, tree):
        """
        Checks the structure of the test tree defined by ttree.
        """
        node = tree.getRoot()
        self.assertEqual(2, node.getChildCnt())
        self.assertEqual(0.6, node.getBranchLen())
        self.assertEqual('a', node.getName())

        node = tree.getRoot().getChild(0)
        self.assertEqual(0, node.getChildCnt())
        self.assertEqual(1.2, node.getBranchLen())
        self.assertEqual('b', node.getName())
        self.assertEqual('a', node.getParent().getName())
        self.assertEqual(0.6, node.getParent().getBranchLen())

        node = tree.getRoot().getChild(1)
        self.assertEqual(0, node.getChildCnt())
        self.assertEqual(2.4, node.getBranchLen())
        self.assertEqual('c', node.getName())
        self.assertEqual('a', node.getParent().getName())
        self.assertEqual(0.6, node.getParent().getBranchLen())

    def test_node(self):
        """
        Tests the default constructor and basic tree node functionality.
        """
        tree = Tree()
        root = tree.getRoot()

        self.assertEqual(0.0, root.getBranchLen())
        self.assertEqual('', root.getName())
        self.assertEqual(0, root.getChildCnt())

        root.setBranchLen(0.6)
        self.assertEqual(0.6, root.getBranchLen())

        root.setName('rootnode')
        self.assertEqual('rootnode', root.getName())

        with self.assertRaises(ValueError):
            root.getChild(0)

        with self.assertRaises(ValueError):
            root.getParent()

        root.addChild('child1', 1.2)
        self.assertEqual(1, root.getChildCnt())

        root.addChild('child2', 3.7)
        self.assertEqual(2, root.getChildCnt())

        child1 = root.getChild(0)
        child2 = root.getChild(1)
        self.assertEqual('child1',root[0].getName())
        self.assertEqual(1.2,root[0].getBranchLen())
        self.assertEqual('child2',root[1].getName())
        self.assertEqual(3.7,root[1].getBranchLen())
        self.assertEqual(0, root[0].getChildCnt())
        self.assertEqual('child1', child1.getName())
        self.assertEqual(1.2, child1.getBranchLen())
        self.assertEqual('child2', child2.getName())
        self.assertEqual(3.7, child2.getBranchLen())
        self.assertEqual(0, child1.getChildCnt())
        self.assertEqual('rootnode', child1.getParent().getName())

    def test_parse(self):
        """
        Tests both parsing factory functions, fromString() and fromFile().
        """
        tree = Tree.fromString(ttree)
        self._checkTestTree(tree)

        scriptdir = os.path.dirname(os.path.abspath(__file__))
        tree = Tree.fromFile(os.path.join(scriptdir, 'test_data/test_tree.new'))
        self._checkTestTree(tree)

    def test_write(self):
        tree = Tree.fromString(ttree)
        treestr = StringIO()

        tree.write(treestr, OutputFormats.NEWICK)
        self.assertEqual(ttree, treestr.getvalue())

    def test_toString(self):
        tree = Tree.fromString(ttree)
        treestr = tree.toString(OutputFormats.NEWICK)
        self.assertEqual(ttree, treestr)

