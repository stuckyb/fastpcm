/*
 * Copyright (C) 2018 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>
#include <pybind11/operators.h>
#include <pybind11/options.h>
#include "tree.hpp"
#include "tree_writers.hpp"
#include "random_tree_generator.hpp"


namespace py = pybind11;
using namespace py::literals;

using NodeType = TreeNodePtrPair;

PYBIND11_MODULE(fastpcm, m) {

    py::class_<NodeType>(m, "TreeNode")
        .def(py::init<>())
	.def("__getitem__", &NodeType::getChild, py::return_value_policy::reference_internal,py::is_operator())
        .def("getName", &NodeType::getName,R"fdoc(
             :brief: Gets the name of this TreeNode.
             :rtype: the message id
        )fdoc")
        .def("setName", &NodeType::setName,R"fdoc(
             :param newname: The new name of the TreeNode
             :brief: Sets the name of this TreeNode.
             :rtype: void
        )fdoc")
        .def("getBranchLen", &NodeType::getBranchLen)
        .def("setBranchLen", &NodeType::setBranchLen,R"fdoc(
             :param branchlen: The new branch length of the TreeNode
             :brief: Sets the branch length from this TreeNode to its parent.
             :rtype: void
        )fdoc")
        .def("addChild", (NodeType & (NodeType:: *)())&NodeType::addChild, py::return_value_policy::reference_internal)
        .def("addChild",
             (NodeType & (NodeType:: *)(const std::string &, double))&NodeType::addChild,
             "name"_a, "branchlen"_a, py::return_value_policy::reference_internal, R"fdoc(
             :param name: The name of the child node.
             :param branchlen: The length of the branch leading from the new node to its parent.
             :brief: | Adds a new child node to this tree node.
                     |
                     | Creates a new child node of this tree node and returns a reference to the new child node.
             :rtype: TreeNodePtrPair
        )fdoc")
        .def("getChildCnt", &NodeType::getChildCnt, py::return_value_policy::copy, R"fdoc(
            :brief: Returns the number of child nodes of this tree node.
            :rtype: std::size_t
        )fdoc")
        .def("getChild", &NodeType::getChild, py::return_value_policy::reference_internal)
        .def("getParent", &NodeType::getParent, py::return_value_policy::reference_internal, R"fdoc(
            :brief: | Gets the parent node of this tree node.
                    |
                    | Returns a reference to the parent node of this tree node.  If this node does not have a parent (i.e., it is the root node of a tree), a range_error exception is thrown.
            :rtype: TreeNodePtrPair
        )fdoc");

    // Supported tree output formats.
    py::enum_<OutputFormats>(m, "OutputFormats")
        .value("TEXT", OutputFormats::TEXT)
        .value("RELATIVE", OutputFormats::RELATIVE)
        .value("NEWICK", OutputFormats::NEWICK);

    // Defines an internal _Tree class.  This provides a base for the "public"
    // Tree class that adds additional functionality in Python.
    py::class_<Tree<NodeType>>(m, "_Tree")
        .def(py::init<>())
        .def_static("fromString", &Tree<NodeType>::fromString, "treestr"_a, py::return_value_policy::move)
        .def_static("fromFile", &Tree<NodeType>::fromFile, "path"_a, py::return_value_policy::move)
        .def("getRoot", &Tree<NodeType>::getRoot, py::return_value_policy::reference_internal)
        .def("print",
             &Tree<NodeType>::print, "tformat"_a=OutputFormats::TEXT,
             py::call_guard<py::scoped_ostream_redirect>()
        )
        .def("_write", [](Tree<NodeType> &tree, py::object io_obj, OutputFormats format) {
            py::scoped_ostream_redirect output{std::cout, io_obj};
            tree.write(std::cout, format);
        })
        .def("toString", &Tree<NodeType>::toString, "tformat"_a=OutputFormats::NEWICK)
        .def("__str__", [](Tree<NodeType> &tree) {
            return tree.toString(OutputFormats::TEXT);
        })
        .def("depth", &Tree<NodeType>::depth, py::return_value_policy::copy);

    py::class_<RandomTreeGenerator<Tree<NodeType>>>(m, "RandomTreeGenerator")
        .def(py::init<>())
        .def(
            "birthDeathTree", &RandomTreeGenerator<Tree<NodeType>>::birthDeathTree,
            "birth_rate"_a=1.0, "death_rate"_a=0.0, "final_tipcnt"_a=10,
            py::return_value_policy::move
        );
}
