from . import fastpcm
from .fastpcm import OutputFormats
from .fastpcm import TreeNode
from .fastpcm import _Tree

# Create a "public" alias for the _Tree class and dynamically extend it.  Note
# that creating a subclass of _Tree does not work, because methods in the
# bindings in _Tree could not return instances of the derived class.
Tree = fastpcm._Tree

def write(self, out, tformat=OutputFormats.NEWICK):
    """
    :brief: Writes the contents of the tree to a specified output.

    :rtype: Either a file path (a string) or a file-like object.
    format: The desired tree output format.

    """
    if isinstance(out, str):
        f_out = open(out, 'w')
    else:
        f_out = out

    # Verify that the output object has a working implementation of write() by
    # attempting to write an empty string.  This is necessary to ensure proper
    # error handling.  Currently (Oct. 30, 2018), passing a file-like object
    # without a working implementation of write() to the C++ binding will cause
    # a hard crash; this simple check prevents that and ensures proper
    # exception handling.
    f_out.write('')

    self._write(f_out, tformat)

def toDendropy(self):
    """
    :brief: Writes the contents of the tree as a Tree instance of the Dendropy library

    :rtype: Dendropy Tree with the same contents as selected tree
    """

    try:
        import dendropy                     
    except ImportError:
        print("The library Dendropy is needed for this function and cannot be found.")

    return dendropy.Tree.get(data=self.toString(OutputFormats.NEWICK),schema='newick')

def toETE(self):
    """
    :brief: Writes the contents of the tree as a Tree instance of the ETE library

    :rtype: ETE Tree with the same contents as selected tree

    """
    try:
        import ete3
    except ImportError:
        print("The library ETE is needed for this function and cannot be found.")
    return ete3.Tree(self.toString(OutputFormats.NEWICK),format=1)

def toBiopython(self):
    """
    :brief: Writes the contents of the tree as a Tree instance of the BioPython library

    :rtype: BioPython Tree with the same contents as selected tree

    """
    try:
        from Bio import Phylo
    except ImportError:
        print("The library biopython (Bio) is needed for this function and cannot be found.")
    try:
        from io import StringIO
    except ImportError:
        print("The library StringIO is needed for this fucntion and cannot be found.")
    return Phylo.read(StringIO(self.toString(OutputFormats.NEWICK)), "newick")

def fromDendropy(tree):
    """
    :brief: Writes the contents of the specified Dendropy Tree instance as a local Tree instance

    :rtype: Tree instance

    """
    try:
        import dendropy                     
    except ImportError:
        print("The library Dendropy is needed for this function and cannot be found.")

    try:
        dendroTree=dendropy.Tree.get(data=tree.as_string(schema="newick"),schema="newick")
        newTree=Tree.fromString(dendroTree.as_string(schema="newick"))
        return newTree
    except:
        print("The parameter is not a valid Dendropy Tree")
    
    
def fromETE(tree):
    """
    :brief: Writes the contents of the specified ETE tree instance as a local Tree instance

    :rtype: Tree instance

    """
    try:
        import ete3                     
    except ImportError:
        print("The library ETE is needed for this function and cannot be found.")

    try:
        eteTree=ete3.Tree(tree.write())
        newTree=Tree.fromString(eteTree.write(format=1, format_root_node=True))
        return newTree
    except:
        print("The parameter is not a valid ETE Tree")

def fromBiopython(tree):
    try:
        from Bio import Phylo
    except ImportError:
        print("The library biopython (Bio) is needed for this function and cannot be found.")
    try:
        from io import StringIO
    except ImportError:
        print("The library StringIO is needed for this fucntion and cannot be found.")
    try:
        handle = StringIO()
        trees = {tree}
        Phylo.NewickIO.write(trees,handle)
        newTree= Tree.fromString(handle.getValue())
        return newTree
    except:
        print("bricked")

Tree.write = write
Tree.toDendropy = toDendropy
Tree.toETE = toETE
Tree.toBiopython = toBiopython

# Create a convenience instance of RandomTreeGenerato
random = fastpcm.RandomTreeGenerator()

