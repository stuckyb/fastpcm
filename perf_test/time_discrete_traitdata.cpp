/*
 * Copyright (C) 2016 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * Tests the running time of DiscreteTraitData.  Specifically, this program
 * tests the time required to append data to a DiscreteTraitData object, since
 * appending requires a non-constant-time index lookup.
 *
 */

#include <cstdint>
#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <chrono>
#include <ctime>
#include <algorithm>
#include <random>
using std::string;
using std::to_string;
using std::cout;
using std::endl;
using std::vector;
using std::map;
using std::unordered_map;
using std::set;
using std::chrono::steady_clock;
using std::min_element;
using std::default_random_engine;
using std::exponential_distribution;
using std::uniform_int_distribution;
using std::time;

// Define a type alias for a time duration that uses floating-point seconds.
using DurationS = std::chrono::duration<double>;

// Define a type alias for a pair of time floating-point values.
using TimePair = std::pair<double, double>;

#include "lib/cxxopts/cxxopts.hpp"

#include "indexedset.hpp"
#include "discrete_traitdata.hpp"


template <typename TraitImpl>
double timeAppend(vector<string> &datavals) {
    TraitImpl dtdata;

    // Time appending all data to the discrete trait data object.
    auto start_time = steady_clock::now();
    
    for (auto &dataval : datavals) {
        dtdata.append(dataval);
    }
    
    auto end_time = steady_clock::now();
    
    return DurationS(end_time - start_time).count();
}


int main(int argc, char *argv[]) {
    cxxopts::Options opts("time_discrete_traitdata",
                          " Tests the running time performance of discrete trait data set implementations.");
    bool verbose = false;
    opts.add_options()
    ("h,help", "Print usage information.")
    ("n,numtests", "The number of test replicates (default = 1).",
     cxxopts::value<unsigned>()->default_value("1"))
    ("t,numtraitvals", "The number of possible trait values (default = 1).",
     cxxopts::value<unsigned>()->default_value("1"))
    ("s,datasize", "The number of data values to append (default = 1).",
     cxxopts::value<unsigned>()->default_value("1"))
    ("i,implementation", "The implementation to use.  Valid values are \"linear\", "
                         "\"map\", \"hash\", and \"hybrid\".",
     cxxopts::value<string>()->default_value("linear"))
    ("v,verbose", "Generate verbose output.", cxxopts::value<bool>(verbose))
    ;

    opts.parse(argc, argv);

    if (opts.count("help")) {
        cout << opts.help() << endl;
        return 0;
    }
    
    const unsigned numtests = opts["numtests"].as<unsigned>();
    const unsigned numtraitvals = opts["numtraitvals"].as<unsigned>();
    const unsigned datasize = opts["datasize"].as<unsigned>();
    const string implementation = opts["implementation"].as<string>();
    
    // Generate the set of random trait values.
    default_random_engine rand_eng(time(0));
    uniform_int_distribution<size_t> udist(0, UINT32_MAX);
    set<u_int32_t> traitvals;
    while (traitvals.size() < numtraitvals) {
        traitvals.insert(udist(rand_eng));
    }

    // Generate the data values.  To generate the required number of data
    // values, we move sequentially through the set of possible trait values,
    // taking each as a data value.  This is repeated until the desired number
    // of data values is generated.
    vector<string> datavals;
    auto tvals_it = traitvals.cbegin();
    while (datavals.size() < datasize) {
        datavals.push_back(to_string(*tvals_it));
        
        if (++tvals_it == traitvals.cend()) {
            tvals_it = traitvals.cbegin();
        }
    }
    
    vector<double> appendtimes;
    
    if (implementation == "linear") {
        // Run the test replicates.
        for (unsigned cnt = 0; cnt < numtests; ++cnt) {
            appendtimes.push_back(
                timeAppend<
                    DiscreteTraitData<IndexedSet_Linear<string>>
                >(datavals));
        }
    } else if (implementation == "map") {
        // Run the test replicates.
        for (unsigned cnt = 0; cnt < numtests; ++cnt) {
            appendtimes.push_back(
                timeAppend<
                    DiscreteTraitData<IndexedSet_AC<map, string>>
                >(datavals));
        }
    } else if (implementation == "hash") {
        // Run the test replicates.
        for (unsigned cnt = 0; cnt < numtests; ++cnt) {
            appendtimes.push_back(
                timeAppend<
                    DiscreteTraitData<IndexedSet_AC<unordered_map, string>>
                >(datavals));
        }
    } else if (implementation == "hybrid") {
        // Run the test replicates.
        for (unsigned cnt = 0; cnt < numtests; ++cnt) {
            appendtimes.push_back(
                timeAppend<
                    DiscreteTraitData<IndexedSet_Hybrid<string>>
                >(datavals));
        }
    } else {
        cout << "Unrecognized implementation name.\n" << endl;
        return 1;
    }
    
    double min_appendtime = *min_element(appendtimes.cbegin(), appendtimes.cend());
    
    if (verbose) {
        cout << "\nMinimum time to append " << datasize << " data values (n=" <<
             numtests << " tests; " << numtraitvals << " unique trait values): "
             << min_appendtime << " seconds\n" << endl;
    } else {
        cout << min_appendtime << endl;
    }
    
    return 0;
}
