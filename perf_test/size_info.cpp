/*
 * Copyright (C) 2016 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * This is a simple program that prints out the static sizes of various data
 * types.  It is intended as an aid in analyzing the memory footprints of
 * custom data structures.
 *
 */

#include <cstddef>
#include <string>
#include <iostream>
#include <memory>
using std::size_t;
using std::string;
using std::unique_ptr;
using std::cout;
using std::endl;

#include "tree.hpp"
#include "treenode_ptrpair.hpp"
#include "treenode_vector.hpp"


int main(int argc, char *argv[]) {
    cout << "short: " << sizeof(short) << " bytes" << endl;
    cout << "int: " << sizeof(int) << " bytes" << endl;
    cout << "long: " << sizeof(long) << " bytes" << endl;
    cout << "size_t: " << sizeof(size_t) << " bytes" << endl;
    cout << "double: " << sizeof(double) << " bytes" << endl;
    cout << "pointer: " << sizeof(int*) << " bytes" << endl;
    cout << "unique_ptr: " << sizeof(unique_ptr<int>) << " bytes" << endl;
    cout << "string: " << sizeof(string) << " bytes" << endl;
    cout << "TreeNodePtrPair: " << sizeof(TreeNodePtrPair) << " bytes" << endl;
    cout << "TreeNodeVector: " << sizeof(TreeNodeVector) << " bytes" << endl;
    
    return 0;
}
