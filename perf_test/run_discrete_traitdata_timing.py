#!/usr/bin/python
"""
Automates testing the running times of discrete trait data set implementations.
"""

from __future__ import print_function
import pexpect
import sys


# Define a table of test programs and data structures to test.  Each test
# program must implement the same command-line interface.  Run any of the test
# programs with the "-h" option for details of the required interface.
testprogs = {
    '../../build/test/perf_test/time_discrete_traitdata':
        ['linear', 'map', 'hash', 'hybrid']
}

# Define the test cases.  The number of traversals is defined according to the
# number of leaf nodes in the test tree, using the formula
# log10(numtraversals * 2.5) = 6 - log10(num_leaf_nodes).
numtraitvals = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 21, 32, 48, 72, 108, 162, 243]
datasizes =    [100000] * len(numtraitvals)

# The total number of test replicates to run for each test case.
numtests = 1000

# Define exclusions.  If the number of trait values equals or exceeds the value
# set for the given data structure name, no tests will be run.
exclusions = {
    # Example.
    #'linear': 1000000
}

# Print the tab-delimited results table header.
print('no. trait vals\tdataset size', end='')
for testprog in sorted(testprogs.keys()):
    for testds in testprogs[testprog]:
        print('\t' + testds, end='')
print()

# Run each test case and print the results.
for numtv, datasize in zip(numtraitvals, datasizes):
    print(str(numtv) + '\t' + str(datasize), end='')
    sys.stdout.flush()

    for testprog in sorted(testprogs.keys()):
        for testds in testprogs[testprog]:
            # Only run the tests if the number of trait values does not exceed
            # the exclusion limit for the data structure type.
            if not(testds in exclusions) or (numtv < exclusions[testds]):
                cmd = (testprog + ' -i ' + testds + ' -n ' + str(numtests) +
                       ' -t ' + str(numtv) + ' -s ' + str(datasize))
                result = pexpect.run(cmd, timeout=None).strip();
                print('\t' + str(result), end='')
            else:
                print('\tN/A', end='')
            sys.stdout.flush()

    print()

