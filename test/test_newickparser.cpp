/*
 * Copyright (C) 2016 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <fstream>
using std::string;
using std::ifstream;

#include "gtest/gtest.h"

#include "treenode_ptrpair.hpp"
#include "tree.hpp"
#include "newickparser.hpp"

/*
 * Define the test fixture class.
 */
class NewickParserTest: public testing::Test {
protected:
    using TreeType = Tree<TreeNodePtrPair>;
    NewickParser<TreeType> parser;
};

/*
 * Define the tests.
 */

/*
 * Test that an "empty" parser behaves as expected.
 */
TEST_F(NewickParserTest, DefaultBehavior) {
    EXPECT_FALSE(parser.hasMoreToParse());
}

/*
 * Test that the non-default constructors work as expected.
 */
TEST_F(NewickParserTest, Constructors) {
    // Test the string constructor.
    NewickParser<TreeType> parser2("(a,b);");
    EXPECT_TRUE(parser2.hasMoreToParse());
    auto tree1 = parser2.getNextTree();
    EXPECT_EQ("a", tree1.getRoot().getChild(0).getName());
    EXPECT_FALSE(parser2.hasMoreToParse());

    // Test the input stream constructor.
    ifstream fin(FASTPCM_TEST_SOURCE_DIR "/test_data/parser_test.txt");
    ASSERT_TRUE(fin);
    
    NewickParser<TreeType> parser3(fin);
    EXPECT_TRUE(parser3.hasMoreToParse());
    auto tree2 = parser3.getNextTree();
    EXPECT_EQ("ab", tree2.getRoot().getName());
    EXPECT_FALSE(parser2.hasMoreToParse());
}

/*
 * Test that parsing an input stream works.
 */
TEST_F(NewickParserTest, ParsesInputStream) {
    // Test the input stream constructor.
    ifstream fin(FASTPCM_TEST_SOURCE_DIR "/test_data/parser_test.txt");
    ASSERT_TRUE(fin);
    
    parser.setInput(fin);
    EXPECT_TRUE(parser.hasMoreToParse());
    auto tree = parser.getNextTree();
    EXPECT_EQ("ab", tree.getRoot().getName());
    EXPECT_FALSE(parser.hasMoreToParse());
}

/*
 * Test that trees are parsed correctly.
 */
TEST_F(NewickParserTest, TreeParsing) {
    // Test a simple tree with a single (root) node.
    parser.setInput("a;");
    auto tree = parser.getNextTree();
    EXPECT_EQ("a", tree.getRoot().getName());
    EXPECT_EQ(0.0, tree.getRoot().getBranchLen());
    EXPECT_EQ(0, tree.getRoot().getChildCnt());
    EXPECT_FALSE(parser.hasMoreToParse());
    
    // Test a tree with a root node and two leaf nodes.
    parser.setInput("(a:1.2,b:2.4)root;");
    tree = parser.getNextTree();
    // Check the root node.
    auto &root = tree.getRoot();
    EXPECT_EQ("root", root.getName());
    EXPECT_EQ(0.0, root.getBranchLen());
    EXPECT_EQ(2, root.getChildCnt());
    // Check the first child.
    EXPECT_EQ("a", root.getChild(0).getName());
    EXPECT_EQ(1.2, root.getChild(0).getBranchLen());
    EXPECT_EQ(0, root.getChild(0).getChildCnt());
    EXPECT_EQ("root", root.getChild(0).getParent().getName());
    EXPECT_EQ(0.0, root.getChild(0).getParent().getBranchLen());
    // Check the second child.
    EXPECT_EQ("b", root.getChild(1).getName());
    EXPECT_EQ(2.4, root.getChild(1).getBranchLen());
    EXPECT_EQ(0, root.getChild(1).getChildCnt());
    EXPECT_EQ("root", root.getChild(1).getParent().getName());
    EXPECT_EQ(0.0, root.getChild(1).getParent().getBranchLen());
    
    EXPECT_FALSE(parser.hasMoreToParse());
    
    // Test a tree with some unnamed nodes and unspecified branch lengths.
    parser.setInput("((a):4.8);");
    tree = parser.getNextTree();
    // Check the root node.
    EXPECT_EQ("", root.getName());
    EXPECT_EQ(0.0, root.getBranchLen());
    EXPECT_EQ(1, root.getChildCnt());
    // Check the child node.
    EXPECT_EQ("", root.getChild(0).getName());
    EXPECT_EQ(4.8, root.getChild(0).getBranchLen());
    EXPECT_EQ(1, root.getChild(0).getChildCnt());
    // Check the grandchild node.
    EXPECT_EQ("a", root.getChild(0).getChild(0).getName());
    EXPECT_EQ(0.0, root.getChild(0).getChild(0).getBranchLen());
    EXPECT_EQ(0, root.getChild(0).getChild(0).getChildCnt());
    EXPECT_FALSE(parser.hasMoreToParse());
}

/*
 * Test parsing a single input source with multiple trees.
 */
TEST_F(NewickParserTest, MultipleTrees) {
    parser.setInput("(a,b)root1; \n(c,d,e)root2;");
    
    EXPECT_TRUE(parser.hasMoreToParse());
    auto tree = parser.getNextTree();
    EXPECT_EQ("root1", tree.getRoot().getName());
    EXPECT_EQ(2, tree.getRoot().getChildCnt());
    
    EXPECT_TRUE(parser.hasMoreToParse());
    tree = parser.getNextTree();
    EXPECT_EQ("root2", tree.getRoot().getName());
    EXPECT_EQ(3, tree.getRoot().getChildCnt());
    
    EXPECT_FALSE(parser.hasMoreToParse());
}

/*
 * Test error reporting.
 */
TEST_F(NewickParserTest, ErrorReporting) {
    // Test that attempting to parse with an "empty" parser (i.e., a parser
    // with no input to parse) results in an exception.
    EXPECT_THROW(parser.getNextTree(), NewickParseError);
    
    // Test that attempting to parse beyond the end of an input source results
    // in an exception.
    parser.setInput("(a,b);");
    parser.getNextTree();
    EXPECT_THROW(parser.getNextTree(), NewickParseError);
    
    // Missing semicolon at the end of tree definition.
    parser.setInput("(a,b) (a,b);");
    EXPECT_THROW(parser.getNextTree(), NewickParseError);
    
    // Verify that we cannot continue parsing after a syntax error...
    EXPECT_THROW(parser.getNextTree(), NewickParseError);
    // ...and that resetting the input source allows parsing to continue.
    parser.setInput("(a,b);");
    EXPECT_NO_THROW(parser.getNextTree());
    
    // No closing parenthesis in internal node definition.
    parser.setInput("(a,b;");
    EXPECT_THROW(parser.getNextTree(), NewickParseError);
    
    // Missing/invalid leaf node names.
    parser.setInput(";");
    EXPECT_THROW(parser.getNextTree(), NewickParseError);
    parser.setInput(":");
    EXPECT_THROW(parser.getNextTree(), NewickParseError);
    parser.setInput("(a,);");
    EXPECT_THROW(parser.getNextTree(), NewickParseError);
    parser.setInput("(a,:);");
    EXPECT_THROW(parser.getNextTree(), NewickParseError);
    
    // Invalid/missing branch lengths.
    parser.setInput("(a,b:);");
    EXPECT_THROW(parser.getNextTree(), NewickParseError);
    parser.setInput("(a,b:1.w);");
    EXPECT_THROW(parser.getNextTree(), NewickParseError);
}
