/*
 * Copyright (C) 2016 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cstdint>
#include <string>
#include <map>
#include <unordered_map>
#include <vector>
#include <stdexcept>
using std::string;
using std::to_string;
using std::map;
using std::unordered_map;
using std::vector;
using std::range_error;

#include "gtest/gtest.h"

#include "indexedset.hpp"


/*
 * Define the test fixture class as a template class so that we can test
 * different IndexedSet implementations.
 */
template <typename IndexedSetType>
class IndexedSetTest: public testing::Test {
public:
    IndexedSetTest() = default;

protected:
    /**
     * @brief Sets up the indexed set for the copy/move operations tests.
     * 
     * @return void
     */
    void setupCopyMoveTest(IndexedSetType &iset) {
        iset.insert("mem0");
        iset.insert("mem1");
        
        checkCopyContents(iset);
    }
    
    /**
     * Checks that the contents of an indexed set match the contents expected
     * for the copy/move operations tests.
     */
    void checkCopyContents(IndexedSetType &iset) {
        EXPECT_EQ(2, iset.size());
        EXPECT_EQ("mem0", iset[0]);
        EXPECT_EQ("mem1", iset[1]);
        EXPECT_EQ(0, iset["mem0"]);
        EXPECT_EQ(1, iset["mem1"]);        
    }

    /**
     * Checks the results of a copy operation.  This should be run after
     * setUpCopyMoveTest() is used to initialize the indexed set iset1.  This
     * method assumes that iset1 was copied to iset2, but if the copy operation
     * behaved as expected, the order of the parameters should not actually
     * matter.
     * 
     * @return void
     */
    void checkCopyResults(IndexedSetType &iset1, IndexedSetType &iset2) {
        // Check the copied-to indexed set.
        checkCopyContents(iset2);

        // Check the copied-from indexed set.  It should be the same.
        checkCopyContents(iset1);
        
        // Verify that changing iset2 does not affect iset1.
        iset2.insert("mem2");
        EXPECT_EQ(3, iset2.size());
        EXPECT_EQ(2, iset1.size());
        EXPECT_EQ("mem2", iset2[2]);
        EXPECT_EQ(2, iset2["mem2"]);
        EXPECT_FALSE(iset1.contains("mem2"));
        EXPECT_FALSE(iset1.contains(2));
    }
    
    /**
     * Checks the results of a move operation.  This should be run after
     * setUpCopyMoveTest() is used to initialize the indexed set member iset1.
     * This method assumes that iset1 was moved to iset2.
     * 
     * @return void
     */
    void checkMoveResults(IndexedSetType &iset1, IndexedSetType &iset2) {
        // Check the moved-to indexed set.
        checkCopyContents(iset2);

        // Check the moved-from indexed set.  It should be valid, but empty.
        EXPECT_EQ(0, iset1.size());
        EXPECT_FALSE(iset1.contains("mem0"));
        EXPECT_FALSE(iset1.contains("mem1"));
        EXPECT_FALSE(iset1.contains(0));
        EXPECT_FALSE(iset1.contains(1));
    }
};

/*
 * Specify the IndexedSet implementations we want to test.  The type being
 * tested is available inside tests as "TypeParam".
 */
using IndexedSetTypes = testing::Types<
    IndexedSet_Linear<string, uint16_t>, IndexedSet_AC<map, string, uint16_t>,
    IndexedSet_AC<unordered_map, string, uint16_t>, IndexedSet_Hybrid<string, uint16_t>
>;
TYPED_TEST_CASE(IndexedSetTest, IndexedSetTypes);


/*
 * Define the tests.
 */

/*
 * Test that an empty indexed set behaves as expected.
 */
TYPED_TEST(IndexedSetTest, DefaultBehavior) {
    TypeParam iset;
    
    EXPECT_EQ(0, iset.size());
    EXPECT_FALSE(iset.contains("mem0"));
    EXPECT_FALSE(iset.contains(0));
}

/*
 * Test insertion/lookup operations.
 */
TYPED_TEST(IndexedSetTest, InsertionLookup) {
    TypeParam iset;
    
    // Generate a large number of unique values.  The string values will start
    // at "8000" to ensure that index values are distinct from member values.
    unsigned num_setvals = 4000;
    vector<string> setvals;
    setvals.reserve(num_setvals);
    for (unsigned cnt = 8000; cnt < (num_setvals + 8000); ++cnt) {
        setvals.push_back(to_string(cnt));
    }
    
    // Test basic insertion/retrieval for each value.
    for (unsigned cnt = 0; cnt < num_setvals; ++cnt) {
        EXPECT_EQ(cnt, iset.size());
        
        iset.insert(setvals[cnt]);
        EXPECT_EQ(cnt + 1, iset.size());
        EXPECT_TRUE(iset.contains(setvals[cnt]));
        EXPECT_TRUE(iset.contains(cnt));
        EXPECT_EQ(cnt, iset[setvals[cnt]]);
        EXPECT_EQ(setvals[cnt], iset[cnt]);
    }
    
    // Call insert() again for each member string to verify that only unique
    // values are inserted.
    for (unsigned cnt = 0; cnt < num_setvals; ++cnt) {
        iset.insert(setvals[cnt]);
        EXPECT_EQ(num_setvals, iset.size());
    }
}

/*
 * Test that the maximum possible number of unique set values is supported and
 * that attempting to insert() beyond this limit is properly handled.
 */
TYPED_TEST(IndexedSetTest, UniqueValuesLimit) {
    TypeParam iset;
    
    // Generate the maximum supported number of unique values.
    unsigned num_setvals = TypeParam::MAX_SET_SIZE;
    vector<string> setvals;
    setvals.reserve(num_setvals);
    for (unsigned cnt = 0; cnt < num_setvals; ++cnt) {
        setvals.push_back(to_string(cnt));
    }
    
    // Insert each value.
    for (unsigned cnt = 0; cnt < num_setvals; ++cnt) {
        EXPECT_EQ(cnt, iset.size());
        
        iset.insert(setvals[cnt]);
        EXPECT_EQ(cnt + 1, iset.size());
        EXPECT_EQ(setvals[cnt], iset[cnt]);
    }
    
    // The next insert() should throw an exception.
    EXPECT_THROW(iset.insert("too_many"), range_error);
}

/*
 * Test the copy constructor and copy assignment operator.
 */
TYPED_TEST(IndexedSetTest, CopyOperations) {
    // Copy construction.
    TypeParam iset1;
    this->setupCopyMoveTest(iset1);
    
    TypeParam iset2(iset1);
    this->checkCopyResults(iset1, iset2);
    
    // Copy assignment.
    TypeParam iset3;
    this->setupCopyMoveTest(iset3);
    
    TypeParam iset4;
    iset4 = iset3;
    this->checkCopyResults(iset3, iset4);
}

/*
 * Test the move constructor and move assignment operator.
 */
TYPED_TEST(IndexedSetTest, MoveOperations) {
    // Move construction.
    TypeParam iset1;
    this->setupCopyMoveTest(iset1);
    
    TypeParam iset2(std::move(iset1));
    this->checkMoveResults(iset1, iset2);
    
    // Move assignment.
    TypeParam iset3;
    this->setupCopyMoveTest(iset3);
    
    TypeParam iset4;
    iset4 = std::move(iset3);
    this->checkMoveResults(iset3, iset4);
}

/*
 * Test error reporting.
 */
TYPED_TEST(IndexedSetTest, ErrorReporting) {
    TypeParam iset;
    
    // Attempt to retrieve non-existant set members.
    EXPECT_THROW(iset[0], range_error);
    EXPECT_THROW(iset["mem0"], range_error);
    
    // Verify that those calls don't throw after adding the set member.
    iset.insert("mem0");
    EXPECT_NO_THROW(iset[0]);
    EXPECT_NO_THROW(iset["mem0"]);
    
    // Make sure that other indices are still invalid.
    EXPECT_THROW(iset[1], range_error);
    EXPECT_THROW(iset["mem1"], range_error);
}
