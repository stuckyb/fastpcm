/*
 * Copyright (C) 2018 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <stdexcept>
using std::string;
using std::range_error;

#include "gtest/gtest.h"

#include "tree.hpp"
#include "treenode_ptrpair.hpp"
#include "treenode_vector.hpp"


// Define a simple test tree.
const string TEST_TREE =
    "((gchild1:0.48,gchild2:0.96)child1:0.6, child2:1.2)root:2.4;";
    
/*
 * Define the test fixture class as a template class so that we can test
 * different concrete tree node implementations.
 */
template <typename NodeType>
class TreeTest: public testing::Test {
public:
    TreeTest() {
    }

protected:
    /**
     * Checks the results of a move operation.
     * 
     * @param tree1 The moved-from tree.
     * @param tree2 The moved-to tree.
     * 
     * @return void
     */
    void checkMoveResults(Tree<NodeType> &tree1, Tree<NodeType> &tree2) {
        // Check the moved-to tree.
        checkParsedTree(tree2);

        // Check the moved-from tree.  It should be valid, but empty.
        auto &root = tree1.getRoot();
        EXPECT_EQ("", root.getName());
        EXPECT_EQ(0, root.getChildCnt());
        EXPECT_THROW(root.getChild(0), range_error);
    }
    
    /**
     * @brief Checks that the tree matches the test Newick-format tree.
     * 
     * @return void
     */
    void checkParsedTree(Tree<NodeType> &tree) {
        EXPECT_EQ("root", tree.getRoot().getName());
        EXPECT_EQ(2.4, tree.getRoot().getBranchLen());
        EXPECT_EQ(2, tree.getRoot().getChildCnt());
        
        // Test the child nodes.
        auto *child = &tree.getRoot()[0];
        EXPECT_EQ("child1", child->getName());
        EXPECT_EQ(0.6, child->getBranchLen());   
        EXPECT_EQ(2, child->getChildCnt());
        EXPECT_EQ("root", child->getParent().getName());
        
        child = &tree.getRoot()[1];
        EXPECT_EQ("child2", child->getName());
        EXPECT_EQ(1.2, child->getBranchLen());    
        EXPECT_EQ(0, child->getChildCnt());
        EXPECT_EQ("root", child->getParent().getName());
        
        // Test the grandchild nodes.
        child = &tree.getRoot()[0];
        EXPECT_EQ("gchild1", child->getChild(0).getName());
        EXPECT_EQ(0.48, child->getChild(0).getBranchLen());
        EXPECT_EQ(0, child->getChild(0).getChildCnt());
        EXPECT_EQ("root", child->getChild(0).getParent().getParent().getName());
        
        EXPECT_EQ("gchild2", child->getChild(1).getName());
        EXPECT_EQ(0.96, child->getChild(1).getBranchLen());
        EXPECT_EQ(0, child->getChild(1).getChildCnt());
        EXPECT_EQ("root", child->getChild(1).getParent().getParent().getName());
    }
    
};

/*
 * Define the tree node types we want to test.  The type being tested is
 * available inside tests as "TypeParam".
 */
using NodeTypes = testing::Types<TreeNodePtrPair, TreeNodeVector>;
TYPED_TEST_CASE(TreeTest, NodeTypes);

/*
 * Define the tests.
 */

/*
 * Test that an "empty" tree behaves as expected.
 */
TYPED_TEST(TreeTest, DefaultBehavior) {
    Tree<TypeParam> emptytree;
    
    EXPECT_EQ("", emptytree.getRoot().getName());
    EXPECT_EQ(0.0, emptytree.getRoot().getBranchLen());
    EXPECT_EQ(0, emptytree.getRoot().getChildCnt());
}

/*
 * Test reading a tree from a tree file.
 */
TYPED_TEST(TreeTest, ParseFromFile) {
    auto tree = Tree<TypeParam>::fromFile(
        FASTPCM_TEST_SOURCE_DIR "/test_data/simple_tree.new"
    );
    this->checkParsedTree(tree);
}

/*
 * Test reading a tree from a string.
 */
TYPED_TEST(TreeTest, ParseFromString) {
    auto tree = Tree<TypeParam>::fromString(TEST_TREE);
    this->checkParsedTree(tree);
}

/*
 * Test that the move constructor behaves correctly.
 */
TYPED_TEST(TreeTest, MoveConstruction) {
    auto tree1 = Tree<TypeParam>::fromString(TEST_TREE);
    
    Tree<TypeParam> tree2(std::move(tree1));
    
    this->checkMoveResults(tree1, tree2);
}

/*
 * Test that move assignment behaves correctly.
 */
TYPED_TEST(TreeTest, MoveAssignment) {
    auto tree1 = Tree<TypeParam>::fromString(TEST_TREE);
    
    Tree<TypeParam> tree2;
    tree2 = std::move(tree1);
    
    this->checkMoveResults(tree1, tree2);
}
