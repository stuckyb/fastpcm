/*
 * Copyright (C) 2018 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <sstream>
#include <stdexcept>
using std::string;
using std::ostringstream;
using std::range_error;

#include "gtest/gtest.h"

#include "tree.hpp"


// Define a simple test tree.
const string TEST_TREE =
    "((gchild1:0.48,gchild2:0.96)child1:0.6, child2:1.2)root:2.4;";
    

/*
 * Define the test fixture class.
 */
class TreeWritersTest: public testing::Test {
public:
    TreeWritersTest() {
        ttree = Tree<>::fromString(TEST_TREE);
    }

protected:
    Tree<> ttree;
};


/*
 * Define the tests.
 */

/*
 * Tests all tree output format implementations through the generic write()
 * method.
 */
TEST_F(TreeWritersTest, TestWrite) {
    ostringstream treestr;
    
    // Test the default output.
    auto newick_exp = "((gchild1:0.48,gchild2:0.96)child1:0.6,child2:1.2)root:2.4;";
    ttree.write(treestr);
    EXPECT_EQ(newick_exp, treestr.str());
    
    // Test Newick output.
    treestr.str("");
    ttree.write(treestr, OutputFormats::NEWICK);
    EXPECT_EQ(newick_exp, treestr.str());
    
    // Test "pretty print" output.
    auto pprint_exp = 
R"(root(ROOT): 2.4
 ├── child1: 0.6
 │   ├── gchild1: 0.48
 │   └── gchild2: 0.96
 └── child2: 1.2)";
    treestr.str("");
    ttree.write(treestr, OutputFormats::TEXT);
    //std::cout << treestr.str();
    EXPECT_EQ(pprint_exp, treestr.str());
}
