/*
 * Copyright (C) 2018 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <stdexcept>
using std::string;
using std::range_error;

#include "gtest/gtest.h"

#include "treenode_ptrpair.hpp"
#include "treenode_vector.hpp"

/*
 * Define the test fixture class as a template class so that we can test
 * different concrete tree node implementations.
 */
template <typename NodeType>
class NodeTest: public testing::Test {
protected:
    /**
     * @brief Sets up nodes for the move operations tests.
     * 
     * @return void
     */
    void setUpMoveTests(NodeType &node1, NodeType &node2, NodeType &node3) {
        node1.setName("no_children");
        node1.setBranchLen(0.61);
        
        node2.setName("1_child");
        node2.setBranchLen(0.62);
        node2.addChild("1_child-child1", 0.8);

        node3.setName("2_children");
        node3.setBranchLen(0.6);
        auto &gchild = node3.addChild("2_children-child1", 0.12);
        node3.addChild("2_children-child2", 0.24);
        gchild.addChild("2_children-gchild1", 0.48);
        gchild.addChild("2_children-gchild2", 0.96);
    }

    /**
     * Checks the results of a move operation on a node with no children.
     * This should be run after setUpMoveTests() is used to initialize the
     * node.  This method assumes that the node "from" was moved to the node
     * "to".
     *
     * @return void
     */
    void checkMoveResults1(NodeType &from, NodeType &to) {
        // Check the moved-to node.
        EXPECT_EQ("no_children", to.getName());
        EXPECT_EQ(0, to.getChildCnt());
        EXPECT_EQ(0.61, to.getBranchLen());
        EXPECT_THROW(to.getParent(), range_error);

        // Check the moved-from node.  It should be valid, but empty.
        EXPECT_EQ("", from.getName());
        EXPECT_EQ(0, from.getChildCnt());
        EXPECT_THROW(from.getChild(0), range_error);
    }
    
    /**
     * Checks the results of a move operation on a node with 1 child.  This
     * should be run after setUpMoveTests() is used to initialize the node.
     * This method assumes that the node "from" was moved to the node "to".
     *
     * @return void
     */
    void checkMoveResults2(NodeType &from, NodeType &to) {
        // Check the moved-to node.
        EXPECT_EQ("1_child", to.getName());
        EXPECT_EQ(1, to.getChildCnt());
        EXPECT_EQ(0.62, to.getBranchLen());
        EXPECT_THROW(from.getChild(1), range_error);
        
        EXPECT_EQ("1_child-child1", to[0].getName());
        EXPECT_EQ(0.8, to[0].getBranchLen());
        EXPECT_EQ("1_child", to[0].getParent().getName());
        EXPECT_EQ(0.62, to[0].getParent().getBranchLen());
        EXPECT_THROW(from.getChild(0), range_error);
        
        // Check the moved-from node.  It should be valid, but empty.
        EXPECT_EQ("", from.getName());
        EXPECT_EQ(0, from.getChildCnt());
        EXPECT_THROW(from.getChild(0), range_error);
    }

    /**
     * Checks the results of a move operation on a node with 2 children and 2
     * grandchildren.  This should be run after setUpMoveTests() is used to
     * initialize the node.  This method assumes that the node "from" was
     * moved to the node "to".
     *
     * @return void
     */
    void checkMoveResults3(NodeType &from, NodeType &to) {
        // Check the moved-to node.
        EXPECT_EQ("2_children", to.getName());
        EXPECT_EQ(2, to.getChildCnt());
        EXPECT_EQ(0.6, to.getBranchLen());
        EXPECT_THROW(from.getChild(2), range_error);
        
        string childnames[] = {"2_children-child1", "2_children-child2"};
        string gchildnames[] = {"2_children-gchild1", "2_children-gchild2"};
        double c_brlens[] = {0.12, 0.24};
        double gc_brlens[] = {0.48, 0.96};

        // Test the child nodes.
        for (unsigned cnt = 0; cnt < 2; ++cnt) {
            auto &cnode = to[cnt];
            EXPECT_EQ(childnames[cnt], cnode.getName());
            EXPECT_EQ(c_brlens[cnt], cnode.getBranchLen());
            EXPECT_EQ("2_children", cnode.getParent().getName());
            EXPECT_EQ(0.6, cnode.getParent().getBranchLen());
        }

        // Test the grandchild nodes.
        for (unsigned cnt = 0; cnt < 0; ++cnt) {
            auto &cnode = to[0].getChild(cnt);
            EXPECT_EQ(gchildnames[cnt], cnode.getName());
            EXPECT_EQ(gc_brlens[cnt], cnode.getBranchLen());
            EXPECT_EQ(to[0].getName(), cnode.getParent().getName());
            EXPECT_EQ("2_children", cnode.getParent().getParent().getName());
            EXPECT_EQ(0, cnode.getChildCnt());
        }
        
        // Check the moved-from node.  It should be valid, but empty.
        EXPECT_EQ("", from.getName());
        EXPECT_EQ(0, from.getChildCnt());
        EXPECT_THROW(from.getChild(0), range_error);
    }

};

/*
 * Define the tree node types we want to test.  The type being tested is
 * available inside tests as "TypeParam".
 */
using NodeTypes = testing::Types<TreeNodePtrPair, TreeNodeVector>;
TYPED_TEST_CASE(NodeTest, NodeTypes);

/*
 * Define the tests.
 */

/*
 * Test that an "empty" node behaves as expected.
 */
TYPED_TEST(NodeTest, DefaultBehavior) {
    TypeParam emptynode;
    
    EXPECT_EQ("", emptynode.getName());
    EXPECT_EQ(0.0, emptynode.getBranchLen());
    EXPECT_EQ(0, emptynode.getChildCnt());
}

/*
 * Test setting node properties.
 */
TYPED_TEST(NodeTest, NodeProperties) {
    TypeParam node("oldname");
    
    EXPECT_EQ("oldname", node.getName());
    EXPECT_EQ(0.0, node.getBranchLen());
    
    node.setName("newname");
    node.setBranchLen(2.6);
    
    EXPECT_EQ("newname", node.getName());
    EXPECT_EQ(2.6, node.getBranchLen());
}

/*
 * Test adding nodes to a tree.
 */
TYPED_TEST(NodeTest, AddingNodes) {
    TypeParam node("root");
    
    string childnames[] = {"child1", "child2", "child3"};
    string gchildnames[] = {"gchild1", "gchild2", "gchild3"};
    double testbrlens[] = {1.2, 2.4, 3.0};
    
    // Test creating nodes that are children of the root node.
    for (unsigned cnt = 0; cnt < 3; ++cnt) {
        EXPECT_EQ(cnt, node.getChildCnt());
        node.addChild(childnames[cnt], testbrlens[cnt]);
        EXPECT_EQ(cnt + 1, node.getChildCnt());
    }
    
    for (unsigned cnt = 0; cnt < 3; ++cnt) {
        auto &cnode = node.getChild(cnt);
        EXPECT_EQ(childnames[cnt], cnode.getName());
        EXPECT_EQ(testbrlens[cnt], cnode.getBranchLen());
        EXPECT_EQ("root", cnode.getParent().getName());
        EXPECT_EQ(0, cnode.getChildCnt());
    }
    
    // Verify that the subscript operator also works as expected.
    for (unsigned cnt = 0; cnt < 3; ++cnt) {
        auto &cnode = node[cnt];
        EXPECT_EQ(childnames[cnt], cnode.getName());
        EXPECT_EQ(testbrlens[cnt], cnode.getBranchLen());
        EXPECT_EQ("root", cnode.getParent().getName());
        EXPECT_EQ(0, cnode.getChildCnt());
    }
    
    // Test creating nodes that are grandchildren of the root node.
    auto &parent = node.getChild(1);
    
    for (unsigned cnt = 0; cnt < 3; ++cnt) {
        EXPECT_EQ(cnt, parent.getChildCnt());
        parent.addChild(gchildnames[cnt], testbrlens[cnt]);
        EXPECT_EQ(cnt + 1, parent.getChildCnt());
    }
    
    for (unsigned cnt = 0; cnt < 3; ++cnt) {
        auto &cnode = parent.getChild(cnt);
        EXPECT_EQ(gchildnames[cnt], cnode.getName());
        EXPECT_EQ(testbrlens[cnt], cnode.getBranchLen());
        EXPECT_EQ(parent.getName(), cnode.getParent().getName());
        EXPECT_EQ("root", cnode.getParent().getParent().getName());
        EXPECT_EQ(0, cnode.getChildCnt());
    }
    
    // Make sure the other child nodes were not affected.
    EXPECT_EQ(0, parent.getChild(0).getChildCnt());
    EXPECT_EQ(0, parent.getChild(2).getChildCnt());
}

/*
 * Test that the move constructor behaves correctly.
 */
TYPED_TEST(NodeTest, MoveConstruction) {
    TypeParam node1, node2, node3;
    this->setUpMoveTests(node1, node2, node3);
    
    TypeParam from_node1(std::move(node1));
    this->checkMoveResults1(node1, from_node1);
    
    TypeParam from_node2(std::move(node2));
    this->checkMoveResults2(node2, from_node2);
    
    TypeParam from_node3(std::move(node3));
    this->checkMoveResults3(node3, from_node3);
}

/*
 * Test that move assignment behaves correctly.
 */
TYPED_TEST(NodeTest, MoveAssignment) {
    TypeParam node1, node2, node3;
    this->setUpMoveTests(node1, node2, node3);
    
    TypeParam from_node1;
    from_node1 = std::move(node1);
    this->checkMoveResults1(node1, from_node1);

    TypeParam from_node2;
    from_node2 = std::move(node2);
    this->checkMoveResults2(node2, from_node2);

    TypeParam from_node3;
    from_node3 = std::move(node3);
    this->checkMoveResults3(node3, from_node3);
}

/*
 * Test error reporting.
 */
TYPED_TEST(NodeTest, ErrorReporting) {
    TypeParam node;
    
    // Traverse to a non-existant parent.
    EXPECT_THROW(node.getParent(), range_error);
    
    // Traverse to non-existant child nodes.
    EXPECT_THROW(node.getChild(0), range_error);
    node.addChild();
    EXPECT_NO_THROW(node.getChild(0));
    EXPECT_THROW(node.getChild(1), range_error);
}
