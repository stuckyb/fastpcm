/*
 * Copyright (C) 2016 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <sstream>
#include <fstream>
using std::string;
using std::istringstream;
using std::ifstream;

#include "gtest/gtest.h"

#include "newicklexer.hpp"

/*
 * Define the test fixture class.
 */
class NewickLexerTest: public testing::Test {
protected:
    NewickLexer lexer;
};

/*
 * Define the tests.
 */

/*
 * Test that an "empty" lexer behaves as expected.
 */
TEST_F(NewickLexerTest, DefaultBehavior) {
    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());
    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());
    EXPECT_EQ(0, lexer.getLineNumber());
    EXPECT_EQ(0, lexer.getColumnNumber());
    
    lexer.setInput("");
    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());
    EXPECT_EQ(0, lexer.getLineNumber());
    EXPECT_EQ(0, lexer.getColumnNumber());
    
    string input = "";
    istringstream iss(input);
    lexer.setInput(iss);
    EXPECT_EQ(0, lexer.getLineNumber());
    EXPECT_EQ(0, lexer.getColumnNumber());
}

/*
 * Test that all single-character lexemes are read correctly.
 */
TEST_F(NewickLexerTest, ReadsSingleTokens) {
    lexer.setInput("():;,");
    
    EXPECT_EQ(Token(Token::LEFT_PAREN, "("), lexer.getNextToken());
    EXPECT_EQ(Token(Token::RIGHT_PAREN, ")"), lexer.getNextToken());
    EXPECT_EQ(Token(Token::COLON, ":"), lexer.getNextToken());
    EXPECT_EQ(Token(Token::SEMICOLON, ";"), lexer.getNextToken());    
    EXPECT_EQ(Token(Token::COMMA, ","), lexer.getNextToken());
    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());
}

/*
 * Test reading unquoted strings.
 */
TEST_F(NewickLexerTest, ReadsUnquotedStrings) {
    lexer.setInput("a bc def A BC DEF 1 2a b3 <>{}*&^%$#@!_-|\\/+=~`?.");
    
    string correctvals[] = {"a", "bc", "def", "A", "BC", "DEF", "1", "2a", "b3",
        "<>{}*&^%$#@!_-|\\/+=~`?."};
    
    // Look for each correct string value.
    for (auto &correctval : correctvals) {
        EXPECT_EQ(Token(Token::STRING, correctval), lexer.getNextToken());
    }

    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());
}

/*
 * Test reading quoted strings, including strings with escape sequences.
 */
TEST_F(NewickLexerTest, ReadsQuotedStrings) {
    lexer.setInput("'' \"\" 'single' \"double\" 's\"in\"gle' \"dou'bl'e\" "
        "'''' \"\"\"\" '''a' 'a''' \"\"\"a\" \"a\"\"\" ' a\tb  c ' "
        "\"\ta  b c\" '[];:,()'");
    
    string correctvals[] = {"single", "double", "s\"in\"gle", "dou'bl'e", "'", "\"",
        "'a", "a'", "\"a", "a\"", " a\tb  c ", "\ta  b c", "[];:,()"};
    
    // Look for each correct string value.
    for (auto &correctval : correctvals) {
        EXPECT_EQ(Token(Token::STRING, correctval), lexer.getNextToken());
    }

    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());
}

/*
 * Test that comments are ignored.
 */
TEST_F(NewickLexerTest, IgnoresComments) {
    lexer.setInput("[] [comment] a [com \t\nment] '[not a comment]' b[comment]c");
    
    string correctvals[] = {"a", "[not a comment]", "b", "c"};
    
    // Look for each correct string value.
    for (auto &correctval : correctvals) {
        EXPECT_EQ(Token(Token::STRING, correctval), lexer.getNextToken());
    }

    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());
}

/*
 * Test that white space is ignored.
 */
TEST_F(NewickLexerTest, IgnoresWhiteSpace) {
    lexer.setInput(" \t\na \n\tb \t\n");
    
    EXPECT_EQ(Token(Token::STRING, "a"), lexer.getNextToken());
    EXPECT_EQ(Token(Token::STRING, "b"), lexer.getNextToken());

    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());
}

/*
 * Test line/column number tracking.
 */
TEST_F(NewickLexerTest, TracksLineColNumbers) {
    EXPECT_EQ(0, lexer.getLineNumber());
    EXPECT_EQ(0, lexer.getColumnNumber());
    
    lexer.setInput("( a 'nameb'\n)[];");
    EXPECT_EQ(1, lexer.getLineNumber());
    EXPECT_EQ(1, lexer.getColumnNumber());
    
    // "("
    lexer.getNextToken();
    EXPECT_EQ(1, lexer.getLineNumber());
    EXPECT_EQ(2, lexer.getColumnNumber());
    
    // "a"
    lexer.getNextToken();
    EXPECT_EQ(1, lexer.getLineNumber());
    EXPECT_EQ(4, lexer.getColumnNumber());
    
    // "nameb"
    lexer.getNextToken();
    EXPECT_EQ(1, lexer.getLineNumber());
    EXPECT_EQ(12, lexer.getColumnNumber());
    
    // ")"
    lexer.getNextToken();
    EXPECT_EQ(2, lexer.getLineNumber());
    EXPECT_EQ(2, lexer.getColumnNumber());
    
    // ";"
    lexer.getNextToken();
    EXPECT_EQ(2, lexer.getLineNumber());
    EXPECT_EQ(5, lexer.getColumnNumber());
    
    // NONE
    lexer.getNextToken();
    EXPECT_EQ(2, lexer.getLineNumber());
    EXPECT_EQ(5, lexer.getColumnNumber());

    lexer.setInput("a");
    EXPECT_EQ(1, lexer.getLineNumber());
    EXPECT_EQ(1, lexer.getColumnNumber());

}

/*
 * Test that the lexer works with an input stream (ifstream, in this case).
 */
TEST_F(NewickLexerTest, ReadsInputStream) {
    ifstream fin(FASTPCM_TEST_SOURCE_DIR "/test_data/parser_test.txt");
    ASSERT_TRUE(fin);
    
    lexer.setInput(fin);
    
    EXPECT_EQ(Token(Token::STRING, "ab"), lexer.getNextToken());
    EXPECT_EQ(Token(Token::COLON, ":"), lexer.getNextToken());
    EXPECT_EQ(Token(Token::STRING, "1.2"), lexer.getNextToken());
    EXPECT_EQ(Token(Token::SEMICOLON, ";"), lexer.getNextToken());
    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());
}

/*
 * Test that the non-default constructors work as expected.
 */
TEST_F(NewickLexerTest, Constructors) {
    // Test the string constructor.
    NewickLexer lexer2("a");
    
    EXPECT_EQ(Token(Token::STRING, "a"), lexer2.getNextToken());
    EXPECT_EQ(Token(Token::NONE, ""), lexer2.getNextToken());

    // Test the input stream constructor.
    ifstream fin(FASTPCM_TEST_SOURCE_DIR "/test_data/parser_test.txt");
    ASSERT_TRUE(fin);

    NewickLexer lexer3(fin);
    
    EXPECT_EQ(Token(Token::STRING, "ab"), lexer3.getNextToken());
    EXPECT_EQ(Token(Token::COLON, ":"), lexer3.getNextToken());
    EXPECT_EQ(Token(Token::STRING, "1.2"), lexer3.getNextToken());
    EXPECT_EQ(Token(Token::SEMICOLON, ";"), lexer3.getNextToken());
    EXPECT_EQ(Token(Token::NONE, ""), lexer3.getNextToken());
}

/*
 * Test error reporting.
 */
TEST_F(NewickLexerTest, ErrorReporting) {
    // Missing comment close.
    lexer.setInput("([a,b);");
    lexer.getNextToken();
    EXPECT_THROW(lexer.getNextToken(), NewickParseError);
    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());    
    
    // Missing end quote.
    lexer.setInput("'name");
    EXPECT_THROW(lexer.getNextToken(), NewickParseError);
    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());    

    // Invalid character.
    lexer.setInput("(a\x07,b);");
    lexer.getNextToken();
    lexer.getNextToken();
    EXPECT_THROW(lexer.getNextToken(), NewickParseError);
    EXPECT_EQ(Token(Token::NONE, ""), lexer.getNextToken());    
}
