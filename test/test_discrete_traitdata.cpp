/*
 * Copyright (C) 2016 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <stdexcept>
using std::string;
using std::range_error;

#include "gtest/gtest.h"

#include "discrete_traitdata.hpp"


/*
 * Define the test fixture class.
 */
class DiscreteTraitDataTest : public testing::Test {
public:
    using dtd_t = DiscreteTraitData<>;
    
    DiscreteTraitDataTest() = default;

protected:
    /**
     * @brief Sets up a DiscreteTraitData for the copy/move operations tests.
     * 
     * @return void
     */
    void setupCopyMoveTest(dtd_t &dtd) {
        dtd.append("tval0");
        dtd.append("tval1");
        dtd.append("tval0");
        
        checkCopyContents(dtd);
    }
    
    /**
     * Checks that the contents of a DiscreteTraitData match the contents
     * expected for the copy/move operations tests.
     */
    void checkCopyContents(dtd_t &dtd) {
        EXPECT_EQ(3, dtd.size());
        EXPECT_EQ("tval0", dtd.traitvalToStr(dtd[0]));
        EXPECT_EQ("tval1", dtd.traitvalToStr(dtd[1]));
        EXPECT_EQ("tval0", dtd.traitvalToStr(dtd[2]));
    }

    /**
     * Checks the results of a copy operation.  This should be run after
     * setUpCopyMoveTest() is used to initialize dtd1.  This method assumes
     * that dtd1 was copied to dtd2, but if the copy operation behaved as
     * expected, the order of the parameters should not actually matter.
     * 
     * @return void
     */
    void checkCopyResults(dtd_t &dtd1, dtd_t &dtd2) {
        // Check the copied-to DiscreteTraitData.
        checkCopyContents(dtd1);

        // Check the copied-from DiscreteTraitData.  It should be the same.
        checkCopyContents(dtd2);
        
        // Verify that changing dtd2 does not affect dtd1.
        dtd2.append("tval2");
        EXPECT_EQ(4, dtd2.size());
        EXPECT_EQ("tval2", dtd2.traitvalToStr(dtd2[3]));
        EXPECT_EQ(3, dtd1.size());
    }
    
    /**
     * Checks the results of a move operation.  This should be run after
     * setUpCopyMoveTest() is used to initialize dtd1.  This method assumes
     * that dtd1 was moved to dtd2.
     * 
     * @return void
     */
    void checkMoveResults(dtd_t &dtd1, dtd_t &dtd2) {
        // Check the moved-to DiscreteTraitData.
        checkCopyContents(dtd2);

        // Check the moved-from DiscreteTraitData.  It should be valid, but
        // empty.
        EXPECT_EQ(0, dtd1.size());
    }
};


/*
 * Define the tests.
 */

/*
 * Test that an empty DiscreteTraitData behaves as expected.
 */
TEST_F(DiscreteTraitDataTest, DefaultBehavior) {
    dtd_t dtd;
    
    EXPECT_EQ(0, dtd.size());
}

/*
 * Test append/lookup operations.
 */
TEST_F(DiscreteTraitDataTest, AppendLookup) {
    dtd_t dtd;
    
    string testdata[] = {
        "tval0", "tval1", "tval3", "tval0", "tval1", "tval3"
    };
    
    unsigned cnt = 0;
    for (auto &testval : testdata) {
        EXPECT_EQ(cnt, dtd.size());
        
        dtd.append(testval);
        EXPECT_EQ(cnt + 1, dtd.size());
        EXPECT_EQ(testval, dtd.traitvalToStr(dtd[cnt]));
        
        ++cnt;
    }
}

/*
 * Test the copy constructor and copy assignment operator.
 */
TEST_F(DiscreteTraitDataTest, CopyOperations) {
    // Copy construction.
    dtd_t dtd1;
    setupCopyMoveTest(dtd1);
    
    dtd_t dtd2(dtd1);
    checkCopyResults(dtd1, dtd2);
    
    // Copy assignment.
    dtd_t dtd3;
    setupCopyMoveTest(dtd3);
    
    dtd_t dtd4;
    dtd4 = dtd3;
    checkCopyResults(dtd3, dtd4);
}

/*
 * Test the move constructor and move assignment operator.
 */
TEST_F(DiscreteTraitDataTest, MoveOperations) {
    // Move construction.
    dtd_t dtd1;
    setupCopyMoveTest(dtd1);
    
    dtd_t dtd2(std::move(dtd1));
    checkMoveResults(dtd1, dtd2);
    
    // Move assignment.
    dtd_t dtd3;
    setupCopyMoveTest(dtd3);
    
    dtd_t dtd4;
    dtd4 = std::move(dtd3);
    checkMoveResults(dtd3, dtd4);
}

/*
 * Test error reporting.
 */
TEST_F(DiscreteTraitDataTest, ErrorReporting) {
    dtd_t dtd;
    
    // Attempt to retrieve non-existant data.
    EXPECT_THROW(dtd[0], range_error);
    EXPECT_THROW(dtd[1], range_error);
    
    // Verify that those calls don't throw after adding data.
    dtd.append("tval0");
    dtd.append("tval0");
    EXPECT_NO_THROW(dtd[0]);
    EXPECT_NO_THROW(dtd[1]);
    
    // Make sure that other indices are still invalid.
    EXPECT_THROW(dtd[2], range_error);
    EXPECT_THROW(dtd[3], range_error);
}
