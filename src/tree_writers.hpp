/*
 * Copyright (C) 2018 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TREE_WRITERS_HPP
#define TREE_WRITERS_HPP

#include <iostream>
#include <string>
#include <stdexcept>
#include <math.h>


/**
 * @brief Define constants for all supported output formats.
 * 
 */
enum class OutputFormats {
    // A "pretty print", text-based format.
    TEXT=0,
    // The standard Newick format.
    NEWICK,
    RELATIVE
};

/*
 * Defines a class for generating text representations of trees.  This class
 * doesn't store any state (nor does it need to), so for now, all methods are
 * implemented as static methods.  Even though we don't instantiate the class,
 * this approach is still preferable to using a namespace, because we can use
 * class template parameters across all member functions.
 */
template <typename TreeType>
class TreeWriters {
public:
    /**
    * @brief "Pretty prints" a Tree data structure.
    * 
    * @param tree The tree to write.
    * @param os Output stream on which to write the tree.
    * @return void
    */
    static void writeText(TreeType &tree, std::ostream &os) {
        auto &root = tree.getRoot();
        os << root.getName() << "(ROOT): " << root.getBranchLen();
        for (size_t cnt = 0; cnt < root.getChildCnt(); ++cnt) {
            _writeNode_Text(root[cnt], os, " ", cnt < (root.getChildCnt() - 1));
        }
    }

    /**
    * @brief Writes a Newick-format representation of a Tree data structure.
    * 
    * @param tree The tree to write.
    * @param os Output stream on which to write the tree.
    * @return void
    */
    static void writeNewick(TreeType &tree, std::ostream &os) {
        _writeNode_Newick(tree.getRoot(), os);
        os << ";" << std::flush;
    }
 
    static void writeRelative(TreeType &tree, std::ostream &os){
        //size_t totalBranchLen=10; //temporarily hard-coded until totalBranchLen function is written
        auto totalBranchLen = tree.depth();
        auto &root = tree.getRoot();
        size_t rootLen=getRelIndex(root.getBranchLen(),totalBranchLen);
//        std::wstring str(rootLen,L'─');
        std::string str =fill(true,rootLen);
        std::string filler=fill(false, rootLen);
        os << str<< root.getName() << "(ROOT): " << root.getBranchLen()<<std::endl;
        for (size_t cnt = 0; cnt < root.getChildCnt(); ++cnt) {
            _writeNode_Relative(root[cnt], os, filler, cnt < (root.getChildCnt() - 1),totalBranchLen);
        }
    }

    static size_t getRelIndex(size_t val, size_t totalBranchLen){
            if(totalBranchLen!=0){
                return floor((val*60)/totalBranchLen);
            }else {
                return 0;
            }

    }

    static auto fill(bool branch,size_t index){
        if(branch){
//            std::wstring filler(index,L'─');
        std::string filler ="";
        for(size_t i=0; i<index;i++){
            filler+="─";
        }
            return filler;
        }else {
            std::string filler(index, ' ');
            return filler;
        }
    }


    /**
     * @brief Writes a text representation of a tree in the specified format.
     * 
     * @param tree The tree to write.
     * @param os Output stream on which to write the tree.
     * @param format The output format. Defaults to Formats::NEWICK.
     * @return void
     */
    static void write(TreeType &tree, std::ostream &os, OutputFormats format=OutputFormats::NEWICK) {
        switch (format) {
        case OutputFormats::TEXT:
            writeText(tree, os);
            break;
        case OutputFormats::NEWICK:
            writeNewick(tree, os);
            break;
        case OutputFormats::RELATIVE:
            writeRelative(tree, os);
            break;
        default:
            throw std::invalid_argument("Invalid output format specified for writing a tree.");
        }
    }

private:
    using NodeType = typename TreeType::NodeType;
    
    static void _writeNode_Text(NodeType &node, std::ostream &os, const std::string &prefix, bool hasmoresibs) {
        os << std::endl;
        if (hasmoresibs) {
            os << prefix << "├── ";
        } else {
            os << prefix << "└── ";
        }
        os << node.getName() << ": " << node.getBranchLen();

        std::string newprefix = prefix;
        if (hasmoresibs) {
            newprefix += "│   ";
        } else {
            newprefix += "    ";
        }

        for (size_t cnt = 0; cnt < node.getChildCnt(); ++cnt) {
            _writeNode_Text(node[cnt], os, newprefix, cnt < (node.getChildCnt() - 1));
        }
    }

    static void _writeNode_Relative(NodeType &node, std::ostream &os, const std::string &prefix, bool hasmoresibs, size_t totalBranchLen) {
        size_t currIndex=getRelIndex(node.getBranchLen(),totalBranchLen);
        std::string str=fill(true,currIndex);
        if (hasmoresibs) {
            os << prefix << "├─"<<str;
        } else {
            os << prefix << "└─"<<str;
        }
        os << node.getName() << ": " << node.getBranchLen()<<std::endl;

        std::string newprefix = prefix;
        std::string fill(currIndex, ' ');
        if (node.getChildCnt()>0&& hasmoresibs) {
            newprefix += "│ "+fill;
        } else {
            newprefix += "  "+fill;
        }

        for (size_t cnt = 0; cnt < node.getChildCnt(); ++cnt) {
            _writeNode_Relative(node[cnt], os, newprefix, cnt < (node.getChildCnt() - 1),totalBranchLen);
        }
        
    }

    static void _writeNode_Newick(NodeType &node, std::ostream &os) {
        if (node.getChildCnt() > 0) {
            os << "(";
            for (size_t cnt = 0; cnt < node.getChildCnt(); ++cnt) {
                _writeNode_Newick(node.getChild(cnt), os);
                if (cnt < (node.getChildCnt() - 1)) {
                    os << ",";
                }
            }
            os << ")";
        }

        os << node.getName() << ":" << node.getBranchLen();
    }
};

#endif // TREE_WRITERS_HPP
