/*
 * Copyright (C) 2016 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NEWICKLEXER_H
#define NEWICKLEXER_H

#include <istream>
#include <string>
#include <sstream>
#include <iomanip>
#include <cctype>
#include <iterator>

#include "newickparse_error.hpp"

struct Token {
    /**
     * @brief A type to indicate the different kinds of tokens.
     * 
     * A type that defines each kind of token the lexer can identify.  Note
     * that this classification does not include a separate token type for
     * floating-point numbers (i.e., branch lengths).  If the lexical grammar
     * distinguished between floating-point number strings and unquoted name
     * strings, it would not be context-free.  For example, given the Newick
     * format grammar, "1.24" could be either a node name or a branch length.
     * Thus, branch lengths and unquoted name strings are both simply
     * identified as "STRING" tokens by the lexer.  Distinguishing between them
     * is left to the parser, which must use the token's context to decide
     * whether it is a name or a number.
     */
    enum TokenType {
        NONE, LEFT_PAREN, RIGHT_PAREN, COLON, SEMICOLON, COMMA, STRING
    };
    
    Token() = default;
    Token(TokenType tkind, const std::string &strval): kind(tkind), sval(strval) {}
    bool operator==(const Token &rhs) const;
    bool operator!=(const Token &rhs) const;
    
    TokenType kind = NONE;
    std::string sval = "";
};

class NewickLexer {
public:
    /**
     * @brief Constructs a lexer with no input source.
     * 
     * Constructs a lexer that does not have any input text to analyze.
     * Calling getNextToken() will return a Token with kind == Token::NONE.
     */
    NewickLexer();
    
    /**
     * @brief Constructs a lexer that analyzes an input string.
     * 
     * Constructs a lexer that analyzes a Newick-format text string.  The
     * contents of the string will be copied by the lexer, so the string
     * provided as an argument to the constructor does not need to persist
     * beyond the constructor call.
     * 
     * @param input A text string in Newick format.
     */
    explicit NewickLexer(const std::string &input);
    
    /**
     * @brief Constructs a lexer that analyzes an input text stream.
     * 
     * Constructs a lexer that analyzes an input stream of Newick-format text.
     * 
     * @param input An input stream of Newick-format text.
     */
    explicit NewickLexer(std::istream &input);
    
    /**
     * @brief Resets the lexer with a new input string.
     * 
     * Sets a new Newick-format text string to use as the source for lexical
     * analysis.  After the call to setInput(), lexical scanning will start at
     * the beginning of the new input string.  The contents of the input string
     * will be copied by the lexer, so the string provided by the caller does
     * not need to persist beyond the call.
     * 
     * @param input A text string in Newick format.
     * @return void
     */
    void setInput(const std::string &input);
    
    /**
     * @brief Resets the lexer with a new input stream.
     * 
     * Sets a new input stream as the source for lexical analysis.  After the
     * call to setInput(), lexical scanning will start at the beginning of the
     * new input stream.
     * 
     * @param input An input stream of Newick-format text.
     * @return void
     */
    void setInput(std::istream &input);
    
    /**
     * @brief Gets the next token from the input source.
     * 
     * Returns the next token read from the input source.  If the lexer reaches
     * the end of the input or a token syntax error was encountered, a Token
     * with kind == Token::NONE is returned.
     * 
     * @return The next token in the input source.
     */
    Token getNextToken();
    
    /**
     * @brief Get the current input line number.
     * 
     * Returns the line of the current position in the input text stream.  This
     * method and getColumnNumber() are implicitly inline.
     */
    unsigned getLineNumber() const {
        return linenum;
    }
    
    /**
     * @brief Get the current input column number.
     * 
     * Returns the column number of the current position in the current line of
     * the input text stream.
     */
    unsigned getColumnNumber() const {
        return colnum;
    }
    
    // The copy constructor and copy assignment operator are defined as deleted
    // (which means the move constructor and move assignment operator are not
    // defined) because we can't always preserve state during a copy/move.
    // Specifically, if the source lexer uses a string source, there is no way
    // to preserve the state of the istringstream iterator if it is not at the
    // beginning of the stream.  We can move (but not explicitly copy) the
    // istringstream to the destination object, but neither moving nor copying
    // the corresponding iterator will work as expected.
    NewickLexer(const NewickLexer&) = delete;
    NewickLexer &operator=(NewickLexer) = delete;

    // Characters that cannot appear in a non-quoted string.
    static const std::string RESERVED_CHARS;
    // Single-character tokens.
    static const std::string SINGLECHAR_TOKENS;

private:
    void initInputStream(std::istream &input);
    void advance();
    Token readSingleCharToken();
    Token readUnquotedStrToken();
    Token readQuotedStrToken(char quotechar);
    std::string charToHexStr(char src) const;
    
    std::istringstream iss;
    std::istreambuf_iterator<char> currpos;
    std::istreambuf_iterator<char> endpos;
    unsigned linenum;
    unsigned colnum;
};

#endif /* NEWICKLEXER_H */
