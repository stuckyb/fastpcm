/*
 * Copyright (C) 2016 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DISCRETE_TRAIT_DATA_HPP
#define DISCRETE_TRAIT_DATA_HPP

#include <vector>
#include <string>
#include <map>
#include <unordered_map>
#include <stdexcept>
#include <limits>

#include "indexedset.hpp"


/**
 * @brief Represents data for a discrete trait.
 *
 * Encapsulates data for a discrete trait along with the set of possible trait
 * values.  Here, a discrete trait is defined as a trait whose values are
 * limited to a finite set of possible values.  Trait values are represented
 * as strings, but internally, the trait data are converted to an integer
 * representation to improve memory and computational efficiency.
 * Conceptually, DiscreteTraitData is used like a list, with new data
 * (observations) appended to the end of the list and the subscript operator,
 * [], used to retrieve data values.
 * 
 * DiscreteTraitData uses an IndexedSet to manage the set of possible trait
 * values.  The specific IndexedSet implementation can be specified as a
 * template parameter.  By default, DiscreteTraitData uses an IndexedSet_Hybrid
 * to manage the possible trait values.  This default was chosen by testing the
 * performance of different IndexedSet implementations.  An alternative
 * IndexedSet implementation can be specified with the IndexedSetType template
 * parameter.
 * 
 * Also, by default, DiscreteTraitData uses 2-byte unsigned integers to
 * represent the trait data internally, which means that DiscreteTraitData can
 * support discrete traits with up to 65,535 unique values.  This default can
 * be changed by specifying the index type of the IndexedSet implementation.
 * 
 * @tparam IndexedSetType An IndexedSet implementation to use internally for
 * managing the set of possible trait values.
 */
template <typename IndexedSetType=IndexedSet_Hybrid<string>>
class DiscreteTraitData {
public:
    /**
     * @brief The type used for integer representation of trait values.
     * 
     * Note that sizeof(val_type) must be <= sizeof(size_t).
     */
    using ival_t = typename IndexedSetType::index_t;
    
    /**
     * @brief The maximum possible number of unique trait values.
     */
    static const ival_t MAX_NUM_TRAITVALS;

    // The default construct/copy/move/destroy operations are all we need.
    DiscreteTraitData() = default;
    DiscreteTraitData(const DiscreteTraitData&) = default;
    DiscreteTraitData(DiscreteTraitData&&) = default;
    DiscreteTraitData &operator=(const DiscreteTraitData&) = default;
    DiscreteTraitData &operator=(DiscreteTraitData&&) = default;
    ~DiscreteTraitData() = default;
    
    /**
     * @brief Appends a new value to the list of trait data.
     *
     * Appends a new observed trait value (provided as a character string) to
     * the list of trait data.
     *
     * @param traitval The observed trait value.
     * @return void
     */
    void append(const std::string &traitval) {
        data.push_back(traitvals.insert(traitval));
    }

    /**
     * @brief Gets the trait data value at a given index.
     * 
     * @return The trait data value at the given index.
     */
    ival_t &operator[](size_t index) {
        if (index < data.size()) {
            return data[index];
        } else {
            throw std::range_error("Subscript out of range for accessing discrete trait data.");
        }
    }

    /**
     * @brief Gets the number of data values for this trait.
     * 
     * @return The number of data values for this trait.
     */
    ival_t size() const {
        return data.size();
    }

    /**
     * @brief Gets the integer equivalent of a string trait value.
     * 
     * Given a string representation of a trait value, gets the integer
     * representation of the trait value.
     * 
     * @param traitval A string trait value.
     * @return The integer equivalent of the trait value.
     */
    ival_t traitvalToInt(const std::string &str_traitval) {
        return traitvals[str_traitval];
    }

    /**
     * @brief Gets the string equivalent of an integer trait value.
     * 
     * Given an integer representation of a trait value, gets the string
     * representation of the trait value.
     * 
     * @param traitval An integer trait value.
     * @return A reference to the string representation of the trait value.
     */
    const std::string &traitvalToStr(ival_t int_traitval) {
        return traitvals[int_traitval];
    }

private:
    IndexedSetType traitvals;
    std::vector<ival_t> data;
};

template <typename IndexedSetType>
const typename DiscreteTraitData<IndexedSetType>::ival_t
    DiscreteTraitData<IndexedSetType>::MAX_NUM_TRAITVALS = IndexedSetType::MAX_SET_SIZE;

    
#endif // DISCRETE_TRAIT_DATA_HPP
