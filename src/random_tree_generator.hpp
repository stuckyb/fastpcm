/*
 * Copyright (C) 2018 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RANDOM_TREE_GENERATOR_HPP
#define RANDOM_TREE_GENERATOR_HPP

#include <cstddef>
#include <iostream>
#include <string>
#include <stdexcept>
#include <sstream>
#include <vector>
#include <random>
#include <ctime>
#include <utility>
#include <chrono>

#include "tree.hpp"

using std::size_t;
using std::cout;
using std::endl;
using std::ostringstream;
using std::vector;
using std::default_random_engine;
using std::exponential_distribution;
using std::uniform_int_distribution;
using std::bernoulli_distribution;


// Define a simple data structure to associate a pointer to a tree node with
// the total path length from the root leading to that node.
struct TreeNodeInfo {
    TreeNodeInfo(TreeNodePtrPair *nptr, double len):
        nodeptr(nptr), pathlen(len) {
    }
    TreeNodePtrPair *nodeptr;
    double pathlen;
};


/*
 * Defines a class for generating random trees.
 */
template <typename TreeType>
class RandomTreeGenerator {
public:
    RandomTreeGenerator() {
	// Seed the PRNG using the "fastest" system clock available.  TODO:
	// Investigate using std::random_device and/or one of the more specific
	// PRNGs (e.g., Mersenne Twister).
        auto rseed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
        rand_eng.seed(rseed);
    }
    
    TreeType birthDeathTree(const double birth_rate=1.0, const double death_rate=0.0, const size_t final_tipcnt=10) {
	// An exponential distribution for sampling waiting times to speciation
	// events.
        exponential_distribution<double> expdist(birth_rate);

        // A uniform distribution for choosing a tip to undergo a speciation or
        // extinction event.
        uniform_int_distribution<size_t> udist(0, 0);

        // A Bernoulli distribution for choosing whether events are speciation
        // or extinction events.
        bernoulli_distribution is_speciation(birth_rate / (birth_rate + death_rate));

        TreeType tree;
        
	// The algorithm for generating random trees requires a vector for
	// keeping track of all tips (leaf nodes) in the tree.  At each step of
	// the algorithm, a waiting time is sampled from the exponential
	// distribution parameterized according to the birth rate, death rate,
	// and the total number of "live" tips.  A tip is randomly selected to
	// undergo either speciation (i.e., addition of 2 child nodes) or
	// extinction.  If the tip speciates, the 2 new child tips are added to
	// the vector (one of which replaces the parent node).  The tips vector
	// also stores the total path length from the root to each tip in the
	// tree.  This information, along with the total time, makes it
	// possible to avoid updating all branch lengths at every step of the
	// algorithm.  Instead, branch lengths are only updated when a lineage
	// speciates, becomes extinct, or reaches the end of the tree.
        vector<TreeNodeInfo> tips;
        tips.reserve(final_tipcnt);
        tips.emplace_back(&tree.getRoot(), 0.0);

	// Keeps track of the index of the first "live" leaf node in the
	// evolving tree.  All leaf nodes located prior to this index will be
	// extinct.
        size_t live_tips_start = 0;
        // Tracks the total elapsed time of the evolutionary process.
        double totaltime = 0.0;
        // Tracks the number of "live" leaf nodes in the evolving tree.
        size_t live_tipcnt = 1;
        
	// Loop until the tree contains the desired number of "live" leaf nodes
	// or all lineages have become extinct.
        while ((live_tipcnt < final_tipcnt) && (live_tipcnt > 0)) {
	    // Update the exponential distribution to reflect the total number
	    // of tips and draw the next time step.
            expdist.param(exponential_distribution<double>::param_type(
                            live_tipcnt * (birth_rate + death_rate))
                        );
            totaltime += expdist(rand_eng);

            // Randomly choose a lineage to undergo speciation or extinction.
            udist.param(uniform_int_distribution<size_t>::param_type(
                            live_tips_start, tips.size() - 1)
                    );
            TreeNodeInfo &tnodeinfo = tips[udist(rand_eng)];
            TreeNodePtrPair &tnode = *(tnodeinfo.nodeptr);

	    // Update the branch length to reflect the time elapsed since the
	    // node was created.
            tnode.setBranchLen(totaltime - tnodeinfo.pathlen);

            if (is_speciation(rand_eng)) {
                // Simulate a speciation event by adding two child nodes.
                tnode.addChild("", 0.0);
                tnode.addChild("", 0.0);

		// Add pointers to the new children to the tips vector,
		// replacing the pointer to the parent in the process.
                tnodeinfo.nodeptr = &tnode.getChild(0);
                tnodeinfo.pathlen = totaltime;
                tips.emplace_back(&tnode.getChild(1), totaltime);

                ++live_tipcnt;
            } else {
		// Make this node extinct by swapping pointers and path lengths
		// with the vector entry for the first "live" leaf node.
                std::swap(tips[live_tips_start], tnodeinfo);

                --live_tipcnt;
                ++live_tips_start;
            }
        }

	// At the end of the main algorithm, all "live" leaf nodes in the tree
	// have branch lengths of 0, so we need to update all of their branch
	// lengths so they are consistent with the total elapsed time.  We also
	// generate names for all leaf nodes.
        size_t ext_nodenum = 1;
        size_t live_nodenum = 1;
        for (size_t index = 0; index < tips.size(); ++index) {
            ostringstream nstr;
            TreeNodePtrPair &currnode = *(tips[index].nodeptr);

            if (index < live_tips_start) {
                nstr << "E_" << ext_nodenum++;
            } else {
                currnode.setBranchLen(totaltime - tips[index].pathlen);
                nstr << "L_" << live_nodenum++;
            }

            currnode.setName(nstr.str());
        }
        
        return tree;
    }

private:
    using NodeType = typename TreeType::NodeType;
    default_random_engine rand_eng;
};

#endif // RANDOM_TREE_GENERATOR_HPP
