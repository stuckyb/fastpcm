/*
 * Copyright (C) 2016 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NEWICKPARSER_H
#define NEWICKPARSER_H

#include <iostream>
#include <string>
#include <stdexcept>
#include <memory>

#include "newicklexer.hpp"
#include "newickparse_error.hpp"

template <typename TreeType>
class NewickParser {
public:
    /**
     * @brief Constructs a parser with no input source.
     * 
     * Constructs a parser with no input text to parse.  Calling getNextTree()
     * will throw a NewickParseError.
     * 
     */
    NewickParser();
    
    /**
     * @brief Constructs a parser that parses a text string.
     * 
     * Constructs a parser with an input string that contains one or more
     * Newick-format tree definitions.
     * 
     * @param input A Newick-format input string.
     */
    NewickParser(const std::string &input);
    
    /**
     * @brief Constructs a parser that parses an input stream.
     * 
     * Constructs a parser with an input stream that contains one or more
     * Newick-format tree definitions.
     * 
     * @param input An input stream to parse.
     */
    NewickParser(std::istream &input);
    
    /**
     * @brief Resets the parser with a new input source.
     * 
     * Resets the parser with a new input source.  Parsing resumes at the
     * beginning of the input string.  The contents of the string will
     * ultimately be copied by the parser (by the lexer, actually), so the
     * source string does not have to persist after the call to setInput().
     * 
     * @param input An text string to parse.
     * @return void
     */
    void setInput(const std::string &input);
    
    /**
     * @brief Resets the parser with a new input source.
     * 
     * Resets the parser with a new input source.  Parsing resumes at the
     * beginning of the input stream.  C++ streams cannot be copied, so the
     * input stream *must* persist until parsing is complete.
     * 
     * @param input An input stream to parse.
     * @return void
     */
    void setInput(std::istream &input);
    
    /**
     * @brief Checks if there more input can be parsed.
     * 
     * If there is more input to parse, and no parser errors have been
     * encountered, returns true.
     * 
     * @return bool
     */
    bool hasMoreToParse();
    
    /**
     * @brief Parse the next tree from the current input source.
     * 
     * @return TreeType
     */
    TreeType getNextTree();
    
    // The copy constructor and copy assignment operator must be defined as
    // deleted (and the move constructor and move assignment operator left as
    // undefined) because NewickLexers do not support these operations.
    NewickParser(const NewickParser&) = delete;
    NewickParser &operator=(NewickParser) = delete;

private:
    using NodeType = typename TreeType::NodeType;
    void advance();
    TreeType parseTree();
    void parseTreeNode(NodeType &node);
    void parseInternalNodeDef(NodeType &node);
    void parseLeafNodeDef(NodeType &node);
    double convertStrToDouble(const std::string &str);
    void syntaxError(const std::string &msg, const std::string &context="");
    
    NewickLexer lexer;
    Token currtoken;
    bool syntax_error;
};

#include "newickparser.tpp"

#endif // NEWICKPARSER_H
