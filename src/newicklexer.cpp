/*
 * Copyright (C) 2016 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "newicklexer.hpp"
#include <iostream>

using std::string;
using std::istream;
using std::istringstream;
using std::ostringstream;
using std::istreambuf_iterator;


/******
 * Members of Token.
 ******/

bool Token::operator==(const Token &rhs) const {
    return (this->kind == rhs.kind) && (this->sval == rhs.sval);
}

bool Token::operator!=(const Token &rhs) const {
    return !(*this == rhs);
}


/******
 * Members of NewickLexer.
 ******/

// Define the characters that cannot appear in a non-quoted string.
const std::string NewickLexer::RESERVED_CHARS = "()[]\"':;,";

// Define the single-character tokens.
const std::string NewickLexer::SINGLECHAR_TOKENS = "():;,";

NewickLexer::NewickLexer():
  iss(), currpos(), endpos(), linenum(0), colnum(0) {
}

NewickLexer::NewickLexer(const string &input):
  iss(input), currpos(), endpos(), linenum(1), colnum(1) {
      initInputStream(iss);
}

NewickLexer::NewickLexer(istream &input):
  iss(), currpos(), endpos(), linenum(1), colnum(1) {
      initInputStream(input);
}

void NewickLexer::setInput(const string &input) {
    // Note that accepting a reference to the source string is acceptable, even for
    // temporary arguments (i.e., rvalues), because the istringstream constructor
    // copies the source string.
    iss.str(input);
    initInputStream(iss);
}

void NewickLexer::setInput(istream &input) {
    iss.str("");
    initInputStream(input);
}

/**
 * @brief Prepares the lexer to read an input source.
 * 
 * Given an input stream, initializes the iterator for reading the stream and
 * initializes the line and column number counters.
 * 
 * @param input The input stream.
 * @return void
 */
void NewickLexer::initInputStream(istream &input) {
    currpos = istreambuf_iterator<char>(input);
    if (currpos != endpos) {
        linenum = colnum = 1;
    } else {
        linenum = colnum = 0;
    }
}

/**
 * @brief Reads the next input character.
 * 
 * Advances the input stream to the next character while keeping track of line
 * number and column position.
 */
void NewickLexer::advance() {
    if (currpos != endpos) {
        if (*currpos == '\n') {
            ++linenum;
            colnum = 1;
        } else {
            ++colnum;
        }
        
        ++currpos;
    }
    
    //std::cout << "line: " << linenum << ", column: " << colnum << std::endl;
}

Token NewickLexer::getNextToken() {
    Token nexttoken;

    // Reading quoted strings can produce empty string tokens if the input
    // text contains "''" or "\"\"".  There is no reason to return these to
    // client code, so the outer while loop will silently discard any empty
    // string tokens.
    while (currpos != endpos && nexttoken.kind == Token::NONE) {
        // Consume any whitespace or comments preceding the next token.
        while (currpos != endpos && 
            (isspace(*currpos) || (*currpos == '['))) {
            if (*currpos == '[') {
                // Consume the comment.
                while (currpos != endpos && *currpos != ']') {
                    advance();
                }
                if (currpos != endpos) {
                    // Consume the closing ']'.
                    advance();
                } else {
                    // ERROR: no closing ']' to end the comment.
                    throw NewickParseError("Missing \"]\" at the end of comment.",
                        getLineNumber(), getColumnNumber());
                }
            } else {
                // Consume the whitespace.
                advance();
            }
        }

        if (currpos != endpos) {
            char nextchr = *currpos;

            if (SINGLECHAR_TOKENS.find(nextchr) != string::npos) {
                nexttoken = readSingleCharToken();
            } else if (nextchr == '\'' || nextchr == '"') {
                nexttoken = readQuotedStrToken(nextchr);
            } else if (isgraph(nextchr)) {
                nexttoken = readUnquotedStrToken();
            } else {
                // ERROR: invalid character.
                // Do not allow further input processing.
                currpos = endpos;
                throw NewickParseError("Invalid character (" + charToHexStr(nextchr) +
                    ") in tree definition.", getLineNumber(), getColumnNumber());
            }
        }
    }

    return nexttoken;
}

Token NewickLexer::readSingleCharToken() {
    Token nexttoken;

    switch (*currpos) {
    case '(':
        nexttoken.kind = Token::LEFT_PAREN;
        break;
    case ')':
        nexttoken.kind = Token::RIGHT_PAREN;
        break;
    case ':':
        nexttoken.kind = Token::COLON;
        break;
    case ';':
        nexttoken.kind = Token::SEMICOLON;
        break;
    case ',':
        nexttoken.kind = Token::COMMA;
        break;
    }

    nexttoken.sval = *currpos;
    advance();

    return nexttoken;
}

Token NewickLexer::readUnquotedStrToken() {
    Token nexttoken;

    while (currpos != endpos &&
           isgraph(*currpos) &&
           (RESERVED_CHARS.find(*currpos) == string::npos)) {
        nexttoken.sval += *currpos;
        advance();
    }

    nexttoken.kind = Token::STRING;

    return nexttoken;
}

Token NewickLexer::readQuotedStrToken(char quotechar) {
    Token nexttoken;

    // Discard the opening quote.
    if (*currpos == quotechar) {
        advance();
    } else {
        // ERROR: No opening quote character.
    }

    bool found_closing_quote = false;
    while (currpos != endpos && !found_closing_quote) {
        if (*currpos == quotechar) {
            // See if this is an escaped quote.
            advance();
            if ((currpos != endpos) &&
                *currpos == quotechar) {
                nexttoken.sval += quotechar;
                advance();
            } else {
                // We reached the end of the quoted string.
                found_closing_quote = true;
            }
        } else {
            nexttoken.sval += *currpos;
            advance();
        }
    }

    if (!found_closing_quote) {
        // Error: no closing quote was found.
        throw NewickParseError("Missing closing quote for quoted name string.",
            getLineNumber(), getColumnNumber(), nexttoken.sval);
    }

    if (nexttoken.sval != "") {
        nexttoken.kind = Token::STRING;
    } else {
        nexttoken.kind = Token::NONE;
    }

    return nexttoken;
}

/**
 * @brief Converts a char to its hex representation.
 * 
 * Converts a char value to a string containing the char's equivalent hexadecimal
 * literal representation.  E.g., 'A' is converted to "'\x41'".  This is used for
 * reporting invalid characters encountered during lexical analysis of tree
 * definition "source code".
 * 
 * @param src The character to convert.
 * @return A string containing src's equivalent hexidecimal character literal.
 */
string NewickLexer::charToHexStr(char src) const {
    ostringstream os;

    os << std::hex << "'\\x";
    os << std::setfill('0') << std::setw(sizeof(char) * 2) << static_cast<int>(src);
    os << "'";
    
    return os.str();
}
