/*
 * Copyright (C) 2018 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TREE_HPP
#define TREE_HPP

#include <utility>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include "tree_writers.hpp"
#include "newickparser.hpp"
#include "treenode_ptrpair.hpp"


/**
 * @brief A basic data type for trees.
 * 
 * Defines a basic data type to represent phylogenetic trees.  Currently, this
 * is little more than a container for the root node of the tree.  Because tree
 * (deep) copy operations will often be expensive, copy construction and
 * assignment are disabled in an effort to promote efficient client code.  It
 * is possible that copy functionality will eventually be needed, but for now,
 * this is not the case.  Move copy and assignment are supported, however.
 * 
 */
template <typename NodeType_=TreeNodePtrPair>
class Tree {
public:
    /**
     * @brief The type of the nodes of this tree.
     * 
     */
    using NodeType = NodeType_;
    
    /**
     * @brief Creates a new Tree object from a tree file in Newick format.
     * 
     * @param path The path of Newick-formatted tree file.
     * @return Tree
     */
    static Tree fromFile(const std::string &path);
    
    /**
     * @brief Creates a new Tree object from a string in Newick format.
     * 
     * @param treestr A string containing a Newick tree definition.
     * @return Tree
     */
    static Tree fromString(const std::string &treestr);
    
    /**
     * @brief Constructs an "empty" tree with only a root node.
     * 
     */
    Tree(): root("", 0) {}
    
    /**
     * @brief Move constructor.
     */
    Tree(Tree &&tree) noexcept = default;
    
    /**
     * @brief Move assignment operator.
     * 
     * @return Tree&
     */
    Tree &operator=(Tree &&rhs) = default;

    // In general, copying trees is expensive, so for now, the copy operations
    // are disabled to promote efficient resource use by client code.  This
    // restriction might need to be relaxed depending on future requirements.
    Tree(const Tree &tree) = delete;
    Tree &operator=(const Tree &rhs) = delete;

    /**
     * @brief Returns a reference to the root node of this tree.
     *
     * @return NodeType&
     */
    NodeType &getRoot() {
        return root;
    }
    
    void print(OutputFormats format=OutputFormats::TEXT) {
        write(std::cout, format);
        std::cout << std::endl;
    }
    
    void write(std::ostream &os, OutputFormats format=OutputFormats::NEWICK) {
        TreeWriters<Tree>::write(*this, os, format);
    }
    
    std::string toString(OutputFormats format=OutputFormats::NEWICK) {
        std::ostringstream treestr;
        write(treestr, format);
        
        return treestr.str();
    }
    
    double depth() {
        return _nodeDepth(root);
    }
    
private:
    NodeType root;
    
    double _nodeDepth(NodeType &node) {
        double maxsize = 0.0;
        double newsize;
        for (size_t cnt = 0; cnt < node.getChildCnt(); ++cnt) {
            newsize = _nodeDepth(node.getChild(cnt));
            if (newsize > maxsize)
                maxsize = newsize;
        }
        
        return maxsize + node.getBranchLen();
    }
};

#include "tree.tpp"

#endif // TREE_HPP
