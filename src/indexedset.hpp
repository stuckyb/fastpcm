/*
 * Copyright (C) 2016 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef INDEXED_SET_HPP
#define INDEXED_SET_HPP

#include <cstdint>
#include <vector>
#include <map>
#include <unordered_map>
#include <stdexcept>
#include <limits>


/**
 * @brief Implements indexed sets.
 *
 * Implements an "indexed set", a data structure in which all items added to
 * the set are assigned a unique integer index and items in the set can easily
 * be accessed using either their original value or their index value.  Another
 * way to think of the data structure is as a bi-directional map which uniquely
 * maps a set of objects to a sequential list of integers that starts with 0.
 * 
 * IndexedSet_Base is an "abstract" base class that implements the public 
 * IndexedSet interface.  It uses the "Curiously Recurring Template Pattern" to
 * avoid the overhead of dynamic binding.  Concrete subclasses must implement
 * the details of how set items are stored and manipulated.  Specifically, they
 * must provide implementations of four methods: findMemberIndex(),
 * getMemberAt(), doInsert(), and getSize().
 * 
 * @tparam IndexType An integer to use for indexing the set members.
 * @tparam Derived A subclass of IndexedSet_Base.
 */
template <typename MemberType, typename IndexType, typename Derived>
class IndexedSet_Base {
public:
    /**
     * @brief The type of set members.
     * 
     */
    using member_t = MemberType;
    
    /**
     * @brief The integer type to use for indexing set members.
     * 
     * Note that sizeof(index_t) should be <= sizeof(size_t).
     */
    using index_t = IndexType;
    
    /**
     * @brief The maximum possible set size.
     */
    static const index_t MAX_SET_SIZE;

    /**
     * @brief Adds a member to the set.
     * 
     * Adds a new member to the set and returns its index.  If the member is
     * already in the set, the index of the member is returned and no chages
     * are made to the set.
     * 
     * @return The index of the set member.
     */
    index_t insert(const member_t &member) {
        index_t index = derived().findMemberIndex(member);
        
        if (index == size()) {
            // The set member is not already in the set.  Make sure we can
            // handle another set member.
            if (index < MAX_SET_SIZE) {
                derived().doInsert(member);
            } else {
                throw std::range_error(
                    "The maximum number of unique indexed set values was exceeded.");
            }
        }
        
        return index;
    }

    /**
     * @brief Gets the set member at a given index.
     * 
     * @return The set member at the given index.
     */
    const member_t &operator[](index_t index) {
        if (index < size()) {
            return derived().getMemberAt(index);
        } else {
            throw std::range_error("Index out of range for accessing set member.");
        }
    }

    /**
     * @brief Gets the index of a given set member.
     * 
     * @return The index of the given set member.
     */
    index_t operator[](const member_t &member) const {
        index_t index = derived().findMemberIndex(member);
        if (index < size()) {
            return index;
        } else {
            throw std::range_error("The specified set member was not found in the set.");
        }
    }
    
    /**
     * @brief Tests whether the set contains the given member.
     * 
     * @return bool
     */
    bool contains(const member_t &member) const {
        return derived().findMemberIndex(member) < size();
    }

    /**
     * @brief Tests whether the set contains a member with the given index.
     * 
     * @return bool
     */
    bool contains(index_t index) {
        return index < size();
    }

    /**
     * @brief Gets the number of set members.
     * 
     * @return The number of set members.
     */
    index_t size() const {
        return derived().getSize();
    }

protected:
    /**
     * @brief Looks up the index of a given set member.
     * 
     * This method must be implemented by concrete child classes.  If the given
     * member is not found in the set, then size() should be returned.
     * 
     * @param member The set member to look up.
     * @return The index of the set member, or size() if the set member could
     * not be found.
     */
    index_t findMemberIndex(const member_t &member) const;
    
    /**
     * @brief Returns a reference to the set member at a given index.
     * 
     * This method must be implemented by concrete child classes.  Note that
     * bounds checking is handled by the public interface code, so
     * implementations of getMemberAt() do not need to check index validity.
     * 
     * @param index A valid index of a set member.
     * @return A reference to the set member at the given index.
     */
    const member_t &getMemberAt(index_t index) const;
    
    /**
     * @brief Inserts a new member into the set.
     * 
     * This method must be implemented by concrete child classes.  Note that
     * the public interface will call this method only if the item is not
     * already in the set, so implementations of doInsert() do not need to
     * check if the new member is already in the set.
     * 
     * @param member The new set member to insert.
     * @return void
     */
    void doInsert(const member_t &member);
    
    /**
     * @brief Returns the size of the set.
     * 
     * This method must be implemented by concrete child classes.
     * 
     * @return The size of the set.
     */
    index_t getSize() const;

private:
    Derived &derived() {
        return *static_cast<Derived*>(this);
    }
    
    const Derived &derived() const {
        return *static_cast<const Derived*>(this);
    }
};

template <typename MemberType, typename IndexType, typename Derived>
const typename IndexedSet_Base<MemberType, IndexType, Derived>::index_t
    IndexedSet_Base<MemberType, IndexType, Derived>::MAX_SET_SIZE =
        std::numeric_limits<index_t>::max();


 /**
 * @brief An IndexedSet implementation based on linear search.
 *
 * A concrete implementation of IndexedSet that uses simple linear search
 * to map set members to their index values.
 */
template <typename MemberType, typename IndexType=uint16_t>
class IndexedSet_Linear : public
    IndexedSet_Base<MemberType, IndexType, IndexedSet_Linear<MemberType, IndexType>>
{
public:
    using typename IndexedSet_Base<
        MemberType, IndexType, IndexedSet_Linear<MemberType, IndexType>
    >::member_t;

    using typename IndexedSet_Base<
        MemberType, IndexType, IndexedSet_Linear<MemberType, IndexType>
    >::index_t;

    // The default construct/copy/move/delete operations are all we need.
    IndexedSet_Linear() = default;
    IndexedSet_Linear(const IndexedSet_Linear&) = default;
    IndexedSet_Linear(IndexedSet_Linear&&) = default;
    IndexedSet_Linear &operator=(const IndexedSet_Linear&) = default;
    IndexedSet_Linear &operator=(IndexedSet_Linear&&) = default;
    ~IndexedSet_Linear() = default;
    
private:
    friend class IndexedSet_Base<
        MemberType, IndexType, IndexedSet_Linear<MemberType, IndexType>
    >;

    index_t findMemberIndex(const member_t &member) const {
        index_t index = 0;

        for (; index < members.size(); ++index) {
            if (members[index] == member) {
                return index;
            }
        }

        return index;
    }
    
    const member_t &getMemberAt(index_t index) const {
        return members[index];
    }
    
    void doInsert(const member_t &member) {
        members.push_back(member);
    }
    
    index_t getSize() const {
        return members.size();
    }
    
    std::vector<member_t> members;
};


/**
 * @brief An IndexedSet implementation based on an associative container.
 *
 * A concrete implementation of IndexedSet that uses one of the unique key
 * associative containers in the standard library (either map<> or
 * unordered_map<>) to map set members to their index values.  To avoid the
 * memory inefficiency of storing two copies of the set members, the set
 * members are stored as keys in the associative container and lookup from
 * integer indexes to the members is implemented by keeping a vector of
 * pointers to the keys in the associative container.
 * 
 * According to the draft C++14 standard
 * (http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2014/n4296.pdf), this
 * should be safe: "The insert and emplace members [of map<>] shall not affect
 * the validity of iterators and references to the container, and the erase
 * members shall invalidate only iterators and references to the erased
 * elements" (page 763), and "The insert and emplace members [of unordered_map]
 * shall not affect the validity of references to container elements, but may
 * invalidate all iterators to the container" (page 774).
 */
template <
    template <typename, typename, typename ...> class Container,
    typename MemberType,
    typename IndexType=uint16_t
>
class IndexedSet_AC : public
    IndexedSet_Base<MemberType, IndexType, IndexedSet_AC<Container, MemberType, IndexType>>
{
public:
    using typename IndexedSet_Base<
        MemberType, IndexType, IndexedSet_AC<Container, MemberType, IndexType>
    >::member_t;

    using typename IndexedSet_Base<
        MemberType, IndexType, IndexedSet_AC<Container, MemberType, IndexType>
    >::index_t;

    // The default construct/copy/move/delete operations are all we need.
    IndexedSet_AC() = default;
    IndexedSet_AC(const IndexedSet_AC&) = default;
    IndexedSet_AC(IndexedSet_AC&&) = default;
    IndexedSet_AC &operator=(const IndexedSet_AC&) = default;
    IndexedSet_AC &operator=(IndexedSet_AC&&) = default;
    ~IndexedSet_AC() = default;
    
private:
    friend class IndexedSet_Base<
        MemberType, IndexType, IndexedSet_AC<Container, MemberType, IndexType>
    >;
    
    index_t findMemberIndex(const member_t &member) const {
        auto iter = memb_to_index.find(member);
        if (iter != memb_to_index.end()) {
            return iter->second;
        } else {
            return getSize();
        }
    }
    
    const member_t &getMemberAt(index_t index) const {
        return *members[index];
    }
    
    void doInsert(const member_t &member) {
        auto res = memb_to_index.insert({member, getSize()});
        members.push_back(&(res.first->first));
    }
    
    index_t getSize() const {
        return members.size();
    }
    
    std::vector<const member_t*> members;
    Container<member_t, index_t> memb_to_index;
};

/**
 * @brief An IndexedSet implementation based on a hybrid lookup strategy.
 *
 * A concrete implementation of IndexedSet that uses a hybrid lookup strategy
 * to map set members to their index values.  A simple linear search is used if
 * there are 10 or fewer set members; a hash table (as implemented by
 * unordered_map<>) is used otherwise.  This strategy was developed by testing
 * the performance of alternative implementations that used either linear
 * search, a balanced binary search tree (as implemented in the standard
 * library's map<>), or a hash table.
 */
template <typename MemberType, typename IndexType=uint16_t>
class IndexedSet_Hybrid : public
    IndexedSet_Base<MemberType, IndexType, IndexedSet_Hybrid<MemberType, IndexType>>
{
public:
    using typename IndexedSet_Base<
        MemberType, IndexType, IndexedSet_Hybrid<MemberType, IndexType>
    >::member_t;

    using typename IndexedSet_Base<
        MemberType, IndexType, IndexedSet_Hybrid<MemberType, IndexType>
    >::index_t;

    // The default construct/copy/move/delete operations are all we need.
    IndexedSet_Hybrid() = default;
    IndexedSet_Hybrid(const IndexedSet_Hybrid&) = default;
    IndexedSet_Hybrid(IndexedSet_Hybrid&&) = default;
    IndexedSet_Hybrid &operator=(const IndexedSet_Hybrid&) = default;
    IndexedSet_Hybrid &operator=(IndexedSet_Hybrid&&) = default;
    ~IndexedSet_Hybrid() = default;
    
private:
    friend class IndexedSet_Base<
        MemberType, IndexType, IndexedSet_Hybrid<MemberType, IndexType>
    >;
    
    index_t findMemberIndex(const member_t &member) const {
        if (getSize() < 11) {
            // If we have 10 or fewer set members, a simple linear search is
            // usually the fastest lookup method.
            index_t index = 0;
            for (; index < getSize(); ++index) {
                if (*members[index] == member) {
                    return index;
                }
            }
            
            return index;
        } else {
            // If the set size is >10, use the hash table for the lookup.
            auto iter = memb_to_index.find(member);
            if (iter != memb_to_index.end()) {
                return iter->second;
            } else {
                return getSize();
            }
        }
    }
    
    const member_t &getMemberAt(index_t index) const {
        return *members[index];
    }
    
    void doInsert(const member_t &member) {
        auto res = memb_to_index.insert({member, getSize()});
        members.push_back(&(res.first->first));
    }
    
    index_t getSize() const {
        return members.size();
    }
    
    std::vector<const member_t*> members;
    std::unordered_map<member_t, index_t> memb_to_index;
};

#endif // INDEXED_SET_HPP
