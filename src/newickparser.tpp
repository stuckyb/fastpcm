/*
 * Copyright (C) 2016 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NEWICKPARSER_TPP
#define NEWICKPARSER_TPP

#include "newickparser.hpp"

using std::string;
using std::istringstream;
using std::cout;
using std::endl;

template <typename TreeType>
NewickParser<TreeType>::NewickParser():
  lexer(), currtoken(), syntax_error(false) {
}

template <typename TreeType>
NewickParser<TreeType>::NewickParser(const string &input):
  lexer(input), currtoken(), syntax_error(false) {
    advance();
}

template <typename TreeType>
NewickParser<TreeType>::NewickParser(std::istream &input):
  lexer(input), currtoken(), syntax_error(false) {
    advance();
}

template <typename TreeType>
void NewickParser<TreeType>::setInput(const string &input) {
    lexer.setInput(input);
    
    // Reset the syntax error flag and get the first token.
    syntax_error = false;
    advance();
}

template <typename TreeType>
void NewickParser<TreeType>::setInput(std::istream &input) {
    lexer.setInput(input);
    
    // Reset the syntax error flag and get the first token.
    syntax_error = false;
    advance();
}

template <typename TreeType>
inline
bool NewickParser<TreeType>::hasMoreToParse() {
    return (currtoken.kind != Token::NONE) && !syntax_error;
}

template <typename TreeType>
inline
void NewickParser<TreeType>::advance() {
    currtoken = lexer.getNextToken();
}

template <typename TreeType>
void NewickParser<TreeType>::syntaxError(const std::string &msg,
                                         const std::string &context) {
    // Set a flag to indicate that a syntax error was encountered.
    syntax_error = true;
    throw NewickParseError(msg, lexer.getLineNumber(), lexer.getColumnNumber(),
                           context);
}

template <typename TreeType>
TreeType NewickParser<TreeType>::getNextTree() {
    if (hasMoreToParse()) {
        return parseTree();
    } else {
        if (syntax_error) {
            throw NewickParseError("Parsing cannot continue because a syntax "
                                   "error was previously encountered.");
        } else {
            // Nothing more to parse, so throw a parse error.
            throw NewickParseError("Expected the beginning of a tree definition, "
                                   "but no input was found.", lexer.getLineNumber(),
                                   lexer.getColumnNumber());
        }
    }
}

template <typename TreeType>
TreeType NewickParser<TreeType>::parseTree() {
    TreeType newtree;
    
    parseTreeNode(newtree.getRoot());
    
    if (currtoken.kind != Token::SEMICOLON) {
        // ERROR: No semicolon at the end of the tree definition.
        syntaxError("Missing \";\" at the end of tree definition.",
                    currtoken.sval);
    }
    advance();
    
    return newtree;
}

template <typename TreeType>
void NewickParser<TreeType>::parseTreeNode(NodeType &node) {
    if (currtoken.kind == Token::LEFT_PAREN) {
        parseInternalNodeDef(node);
    } else {
        parseLeafNodeDef(node);
    }
    
    if (currtoken.kind == Token::COLON) {
        // The next token has to be a branch length.
        advance();
        node.setBranchLen(convertStrToDouble(currtoken.sval));
        advance();
    }
}
    
template <typename TreeType>
void NewickParser<TreeType>::parseInternalNodeDef(NodeType &node) {
    if (currtoken.kind != Token::LEFT_PAREN) {
        // ERROR: No opening parenthesis.  The logic of parseTreeNode() should
        // ensure that this condition never happens, but the check is included
        // here for completeness.
        syntaxError("\"(\" expected but \"" + currtoken.sval + "\" was encountered.");
    }
    advance();

    parseTreeNode(node.addChild());

    while (currtoken.kind == Token::COMMA) {
        advance();
        parseTreeNode(node.addChild());
    }
    
    if (currtoken.kind != Token::RIGHT_PAREN) {
        // ERROR: No closing parenthesis.
        syntaxError("\")\" expected but \"" + currtoken.sval + "\" was encountered.");
    }
    advance();
    
    if (currtoken.kind == Token::STRING) {
        node.setName(currtoken.sval);
        advance();
    }
}

template <typename TreeType>
void NewickParser<TreeType>::parseLeafNodeDef(NodeType &node) {
    if (currtoken.kind == Token::STRING) {
        node.setName(currtoken.sval);
        advance();
    } else {
        // ERROR: Missing leaf node name.
        syntaxError("Leaf node name expected but \"" + currtoken.sval + "\" was encountered.");
    }
}

template <typename TreeType>
double NewickParser<TreeType>::convertStrToDouble(const string& str) {
    double dval = 0.0;
    std::size_t pos;
    
    if (currtoken.kind == Token::STRING) {
        try {
            dval = std::stod(currtoken.sval, &pos);
        } catch (std::invalid_argument err) {
            syntaxError("Illegal characters in the branch length \"" +
                        currtoken.sval + "\".");
        } catch (std::out_of_range err) {
            syntaxError("Branch length \"" + currtoken.sval +
                        "\" cannot be represented as a double-precision floating point value.");
        }
        if (pos != currtoken.sval.size()) {
            syntaxError("Illegal characters in the branch length \"" +
                        currtoken.sval + "\".");
        }
    } else {
        syntaxError("Missing branch length.");
    }
        
    return dval;
}

#endif // NEWICKPARSER_TPP
