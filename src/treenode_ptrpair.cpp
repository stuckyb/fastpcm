/*
 * Copyright (C) 2016 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "treenode_ptrpair.hpp"

using std::string;
using std::unique_ptr;

TreeNodePtrPair &TreeNodePtrPair::do_addChild(const string &name, double branchlen) {
    // Get (a pointer to) the smart pointer to which the new node will be assigned.
    unique_ptr<TreeNodePtrPair> *uptr;
    if (num_children > 0) {
        uptr = &(getChild(num_children - 1).right_sib);
    } else {
        uptr = &first_child;
    }
    
    // Create the new node.
    *uptr = std::make_unique<TreeNodePtrPair>(name, branchlen, this);
    
    ++num_children;
    
    return **uptr;
}

void TreeNodePtrPair::_updateChildLinks() {
    if (num_children > 0) {
        TreeNodePtrPair *curr_child = first_child.get();
        curr_child->parent = this;
        for (size_t cnt = 1; cnt < num_children; ++cnt) {
            curr_child = curr_child->right_sib.get();
            curr_child->parent = this;
        }
    }
}
