/*
 * Copyright (C) 2016 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "newickparse_error.hpp"

NewickParseError::NewickParseError(const std::string &message, unsigned linenum,
                                   unsigned colnum, const std::string &context):
  msg(), line(linenum), col(colnum), context(context) {
    std::ostringstream os;
    
    os << "Tree syntax error detected at line " << linenum << ", column " << colnum;
    if (context != "") {
        os << ", near \"" << context << "\"";
    }
    os << ": " << message;
    
    msg = os.str();
}

const char *NewickParseError::what() const throw() {
    return msg.c_str();
}
