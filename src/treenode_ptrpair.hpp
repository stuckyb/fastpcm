/*
 * Copyright (C) 2016 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TREENODE_PTRPAIR_HPP
#define TREENODE_PTRPAIR_HPP

#include <cstddef>
#include <memory>
#include <stdexcept>

#include "treenode.hpp"


/**
 * @brief Provides a concrete implementation of a tree node.
 * 
 * Provides a concrete implementation of a tree node based on the
 * "first child"/"right sibling" strategy.  Each node holds a pointer to its
 * first child (i.e., the child at index 0) and its right sibling (i.e., the
 * sibling at index 1 greater than the index of the current node).
 * 
 */
class TreeNodePtrPair : public TreeNode<TreeNodePtrPair> {
public:
    TreeNodePtrPair(const std::string &name = "", double branchlen = 0.0,
                    TreeNodePtrPair *parentptr = nullptr):
        TreeNode<TreeNodePtrPair>(name, branchlen, parentptr),
        num_children(0),
        first_child(nullptr),
        right_sib(nullptr) {}
    
    /**
     * @brief Move constructor.
     * 
     * We can't use the synthesized move constructor because it would leave the
     * moved-from node in an inconsistent state (num_children, specifically)
     * and pointers from child nodes to the parent node (this node) would be
     * invalid.
     */
    TreeNodePtrPair(TreeNodePtrPair &&node) noexcept:
      TreeNode<TreeNodePtrPair>(std::move(node)),
      num_children(node.num_children),
      first_child(std::move(node.first_child)),
      right_sib(std::move(node.right_sib)) {
        // Set the moved-from node to a valid, empty state.
        node.num_children = 0;
        
        _updateChildLinks();
    }
    
    /**
     * @brief Move assignment operator.
     * 
     * We can't use the synthesized move assignment operator because it would
     * leave the moved-from node in an inconsistent state (num_children,
     * specifically) and pointers from child nodes to the parent node (this
     * node) would be invalid.
     * 
     * @return TreeNodePtrPair&
     */
    TreeNodePtrPair &operator=(TreeNodePtrPair &&rhs) {
        // Move the base class members.
        TreeNode<TreeNodePtrPair>::operator=(std::move(rhs));
        
        num_children = rhs.num_children;
        first_child = std::move(rhs.first_child);
        right_sib = std::move(rhs.right_sib);
        
        // Set the moved-from node to a valid, empty state.
        rhs.num_children = 0;
        
        _updateChildLinks();
        
        return *this;
    }

private:
    friend class TreeNode<TreeNodePtrPair>;
    
    TreeNodePtrPair &do_addChild(const std::string &name, double branchlen);
    
    TreeNodePtrPair &do_getChild(std::size_t index) {
        // The base class takes care of bounds checking on index, so here, we
        // just assume that index is valid.
        TreeNodePtrPair *curr_child = first_child.get();
        for (size_t cnt = 0; cnt < index; ++cnt) {
            curr_child = curr_child->right_sib.get();
        }

        return *curr_child;
    }

    std::size_t do_getChildCnt() const {
        return num_children;
    }
    
    /**
     * @brief Updates links (pointers) from child nodes to this node after a
     * move operation.
     * 
     * @return void
     */
    void _updateChildLinks();

    std::size_t num_children = 0;
    std::unique_ptr<TreeNodePtrPair> first_child;
    std::unique_ptr<TreeNodePtrPair> right_sib;
};

#endif // TREENODE_PTRPAIR_HPP
