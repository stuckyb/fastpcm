/*
 * Copyright (C) 2016 Brian J. Stucky
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef NEWICKPARSE_ERROR_H
#define NEWICKPARSE_ERROR_H

#include <stdexcept>
#include <string>
#include <sstream>

/**
 * @brief Exception class for tree syntax errors.
 * 
 * A simple exception class for reporting syntax errors encountered when
 * reading Newick-format tree definitions.
 */
class NewickParseError: public std::exception {
public:
    /**
     * @brief Constructs a NewickParseError with a simple error message.
     */
    explicit NewickParseError(const std::string &message):
      msg(message), line(0), col(0), context() {
    }
    
    /**
     * @brief Constructs a NewickParseError that reports error location.
     * 
     * Constructs a NewickParseError with a detailed error message that includes
     * the line number and column at which the error was detected and a custom
     * error message.  If applicable, a string containing the context of the
     * error in the input source can also be included.
     * 
     * @param message A custom message describing the error.
     * @param linenum Input line number at which the error was detected.
     * @param colnum Input column at which the error was detected.
     * @param context The context of the error in the input source.
     */
    explicit NewickParseError(const std::string &message, unsigned linenum,
                              unsigned colnum, const std::string &context="");
    
    /**
     * @brief Returns the error message.
     * 
     * Returns the error message associated with the exception.  If the line
     * number and column were provided, they will be included in the message.
     * 
     * @return The error message.
     */
    virtual const char *what() const throw();

protected:
    std::string msg;
    unsigned line, col;
    std::string context;
};

#endif /* NEWICKPARSE_ERROR_H */
