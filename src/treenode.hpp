/*
 * Copyright (C) 2016 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TREENODE_HPP
#define TREENODE_HPP

#include <cstddef>
#include <stdexcept>


/**
 * @brief Defines the interface and common functionality for tree node objects.
 * 
 * This class acts as an "abstract" base class for all concrete tree node
 * implementations.  It implements static polymorphism via the so-called
 * "Curiously Recurring Template Pattern".  To implement a concrete tree node
 * type, subclasses need to define three private methods -- do_addChild(),
 * do_getChildCnt(), and do_getChild() -- and the subclass needs to also
 * declare this base class as a friend.  Depending on implementation details,
 * base classes might also need to define non-synthesized versions of the move
 * constructor and move assignment operator.  To avoid dangling pointers to
 * parent nodes, concrete tree node implementations must ensure that when a
 * node is deleted, all of its child nodes are also deleted.
 * 
 */
template <typename Derived>
class TreeNode {
public:
    /**
     * @brief Constructs a TreeNode.
     * 
     * @param name The name of the new node (default = "").
     * @param branchlen The branch length of the new node (default = 0.0).
     * @param parentptr A pointer to the parent of the new node
     *                  (default = nullptr).
     */
    TreeNode(const std::string &name = "", double branchlen = 0.0,
             Derived *parentptr = nullptr):
        name_(name),
        branchlength(branchlen),
        parent(parentptr) {}

    /**
     * @brief Move constructor.
     */
    TreeNode(TreeNode &&node) noexcept = default;
    
    /**
     * @brief Move assignment operator.
     *
     * @return TreeNode&
     */
    TreeNode &operator=(TreeNode &&rhs) = default;

    // (Deep) copying tree nodes will often be expensive, so for now, the copy
    // operations are disabled to promote efficient resource use by client
    // code.  This restriction might need to be relaxed in the future depending
    // on client code requirements.
    TreeNode(const TreeNode &node) = delete;
    TreeNode &operator=(const TreeNode &rhs) = delete;

    /**
     * @brief Gets the name of this TreeNode.
     *
     * @return const std::string&
     */
    const std::string &getName() const {
        return name_;
    }

    /**
     * @brief Sets the name of this TreeNode.
     * 
     * @return void
     */
    void setName(const std::string &newname) {
        name_ = newname;
    }

    /**
     * @brief Gets the branch length from this TreeNode to its parent.
     * 
     * @return double
     */
    double getBranchLen() const {
        return branchlength;
    }

    /**
     * @brief Sets the branch length from this TreeNode to its parent.
     * 
     * @return void
     */
    void setBranchLen(double branchlen) {
        branchlength = branchlen;
    }

    /**
     * @brief Adds a new child node to this tree node.
     * 
     * Creates a new child node of this tree node and returns a reference to
     * the new child node.
     * 
     * @param name The name of the child node.
     * @param branchlen The length of the branch leading from the new node to
     *                  its parent.
     * @return Derived&
     */
    Derived &addChild(const std::string &name, double branchlen) {
        return derived().do_addChild(name, branchlen);
    }

    /**
     * @brief Adds a new child node to this tree node.
     * 
     * Creates a new child node of this tree node with an empty name string and
     * a branch length of 0.0.  Equivalent to addChild("", 0.0).
     * 
     * @return Derived&
     */
    Derived &addChild() {
        return addChild("", 0.0);
    }

    /**
     * @brief Returns the number of child nodes of this tree node.
     * 
     * @return std::size_t
     */
    std::size_t getChildCnt() const {
        return derived().do_getChildCnt();
    }
    
    /**
     * @brief Gets the child node at the specified index.
     * 
     * Returns a reference to the child node at the specified index.  If index
     * is invalid (i.e., if index >= the total number of child nodes), a
     * range_error exception is thrown.
     * 
     * @return Derived&
     */
    Derived &getChild(std::size_t index) {
        if (index >= getChildCnt()) {
            throw std::range_error("Subscript out of range for accessing child node.");
        }

        return derived().do_getChild(index);
    }

    /**
     * @brief Gets the child node at the specified index.
     * 
     * Returns a reference to the child node at the specified index.
     * Equivalent to getChild(index).
     * 
     * @return Derived&
     */
    Derived &operator[](std::size_t index) {
        return derived().getChild(index);
    }

    /**
     * @brief Gets the parent node of this tree node.
     * 
     * Returns a reference to the parent node of this tree node.  If this node
     * does not have a parent (i.e., it is the root node of a tree), a
     * range_error exception is thrown.
     * 
     * @return Derived&
     */
    Derived &getParent() {
        if (parent != nullptr) {
            return *parent;
        } else {
            throw std::range_error("Attempt to access parent of a root tree node.");
        }
    }

protected:
    // The name of this child node.
    std::string name_;
    // The length of the branch leading from this node to its parent node.
    double branchlength;
    // A pointer to the parent of this TreeNode.  A raw pointer is safe here
    // because deleting the parent node will automatically delete the child
    // node, so there is no risk of ending up with a dangling pointer.
    Derived *parent;

private:
    Derived &derived() {
        return *static_cast<Derived*>(this);
    }
    
    const Derived &derived() const {
        return *static_cast<const Derived*>(this);
    }
};

#endif // TREENODE_HPP
