/*
 * Copyright (C) 2018 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TREE_TPP
#define TREE_TPP

#include "tree.hpp"

using std::ifstream;

template <typename NodeType_>
Tree<NodeType_> Tree<NodeType_>::fromFile(const std::string &path) {
    ifstream fin(path);
    
    NewickParser<Tree> parser(fin);
    auto new_tree = parser.getNextTree();
    
    return new_tree;
}

template <typename NodeType_>
Tree<NodeType_> Tree<NodeType_>::fromString(const std::string &treestr) {
    NewickParser<Tree> parser(treestr);
    auto new_tree = parser.getNextTree();
    
    return new_tree;
}

#endif // TREE_TPP
