/*
 * Copyright (C) 2016 Brian J. Stucky.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TREENODE_VECTOR_H
#define TREENODE_VECTOR_H

#include <string>
#include <vector>
#include <stdexcept>

#include "treenode.hpp"


/**
 * @brief Provides a concrete implementation of a tree node.
 * 
 * Provides a concrete implementation of a tree node in which each node holds a
 * vector that contains the node's child nodes.
 * 
 */
class TreeNodeVector : public TreeNode<TreeNodeVector> {
public:
    TreeNodeVector(const std::string &name="", double branchlen=0.0,
                    TreeNodeVector *parentptr=nullptr):
        TreeNode<TreeNodeVector>(name, branchlen, parentptr),
        children() {}
    
    /**
     * @brief Move constructor.
     * 
     * We can't use the synthesized move constructor because pointers from
     * child nodes to the parent node (this node) would be invalid.
     */
    TreeNodeVector(TreeNodeVector &&node) noexcept:
      TreeNode<TreeNodeVector>(std::move(node)),
      children(std::move(node.children)) {
        _updateChildLinks();
    }

    /**
     * @brief Move assignment operator.
     * 
     * We can't use the synthesized move assignment operator because pointers
     * from child nodes to the parent node (this node) would be invalid.
     */
    TreeNodeVector &operator=(TreeNodeVector &&rhs) {
        // Move the base class members.
        TreeNode<TreeNodeVector>::operator=(std::move(rhs));
        
        children = std::move(rhs.children);
        _updateChildLinks();
    }

private:
    friend class TreeNode<TreeNodeVector>;
    
    TreeNodeVector &do_addChild(const std::string &name, double branchlen) {
        children.emplace_back(name, branchlen, this);
        return children.back();
    }
    
    TreeNodeVector &do_getChild(size_t index) {
        // The base class takes care of bounds checking on index, so here, we
        // just assume that index is valid.
        return children[index];
    }
    
    std::size_t do_getChildCnt() const {
        return children.size();
    }

    /**
     * @brief Updates links (pointers) from child nodes to this node after a
     * move operation.
     * 
     * @return void
     */
    void _updateChildLinks() {
        for (auto &child : children) {
            child.parent = this;
        }
    }

    std::vector<TreeNodeVector> children;
};

#endif // TREENODE_VECTOR_H
